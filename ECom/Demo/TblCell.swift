//
//  TblCell.swift
//  ECom
//
//  Created by Nan on 24/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class TblCell: UITableViewCell {
    @IBOutlet weak var collect:UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
extension TblCell:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collCell", for: indexPath) as! collCell
        cell.lbl.text = "New Lbl"
        
        return cell
    }
    
    
}
