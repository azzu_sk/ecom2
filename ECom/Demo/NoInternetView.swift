//
//  NoInternetView.swift
//  ECom
//
//  Created by Nan on 18/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Reachability

class NoInternetView: UIViewController{
    
    let network = NetworkManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // If the network is reachable show the main controller
//        network.reachability.whenReachable = { _ in
//            self.showMainController()
//        }
//
//        network.reachability.whenUnreachable = { reachability in
//            self.showOfflinePage()
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        network.reachability.whenReachable = { _ in
            self.showMainController()
        }
        
        network.reachability.whenUnreachable = { reachability in
            self.showOfflinePage()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func showMainController() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "online", sender: self)
//            self.redirect(to: "DidLoadView")
        }
    }
    
    private func showOfflinePage() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "offline", sender: self)
//            self.redirect(to: "NoInternetView")
        }
    }
    
    func redirect(to : String) {
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
}

