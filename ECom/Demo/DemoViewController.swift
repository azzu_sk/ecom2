//
//  DemoViewController.swift
//  ECom
//
//  Created by Nan on 24/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class DemoViewController: UIViewController {

    @IBOutlet weak var tblView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblView.dataSource = self
        tblView.delegate = self
    }

}
extension DemoViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TblCell") as! TblCell
        return cell
    }
    
    
}
