//
//  DidLoadView.swift
//  ECom
//
//  Created by Nan on 11/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Reachability

class DidLoadView: UIViewController{

    @IBOutlet weak var activityVC: NVActivityIndicatorView!
    let network: NetworkManager = NetworkManager.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NetworkManager.isUnreachable { _ in
            self.showOfflinePage()
        }
        NetworkManager.isReachable { _ in
            self.showMainPage()
        }
    }
}

extension DidLoadView : NVActivityIndicatorViewable{
    private func showOfflinePage() -> Void {
        DispatchQueue.main.async {
            self.redirect(to: "NoInternetView")
        }
    }
    private func showMainPage() -> Void {
        DispatchQueue.main.async {
            //self.performSegue(withIdentifier: "MainController", sender: self)
            NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE = "Loading . . ."//NVActivityIndicatorView_Message
            NVActivityIndicatorView.DEFAULT_TYPE = .circleStrokeSpin
            NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 25, height: 25)
            NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 12)
            self.startAnimating()
        }
    }
    
    func redirect(to : String) {
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
