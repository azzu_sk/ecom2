//
//  SelectProductCatViewController.swift
//  ECom
//
//  Created by Nan on 01/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class SelectProductCatViewController: UIViewController {
    
    @IBOutlet weak var tblView:UITableView!
    
    var items = ["Electronic","Tv's & Application", "Fashion", "Home & Furniture", "Toys & Baby", "Beauty & Personal care", "Sports, Books & More", "Game Zone", "Gift Card"]
    var selectItem = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
extension SelectProductCatViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductCatCell") as! SelectProductCatCell
        cell.lbl.text = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductCatCell", for: indexPath) as! SelectProductCatCell
        selectItem = items[indexPath.row]
        let c = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = c.instantiateViewController(withIdentifier: "AddProductViewController") as? AddProductViewController
        vc?.data1 = selectItem
        present(vc!, animated: true, completion: nil)
    }
    
}
