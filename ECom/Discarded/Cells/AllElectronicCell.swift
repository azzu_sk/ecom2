//
//  AllElectronicCell.swift
//  ECom
//
//  Created by Nan on 29/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class AllElectronicCell: UITableViewCell {

    @IBOutlet weak var lbladdnremove: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var tblView:UITableView!
    
    var items = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension AllElectronicCell:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExpandedCell") as! ExpandedCell
        cell.lbl.text = items[indexPath.row]
        return cell
    }
    
    
}
