//
//  TimerViewController.swift
//  ECom
//
//  Created by Nan on 05/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {
    
    @IBOutlet weak var lblTimer:UILabel!
    var timer: Timer?
    var totalTime = 60
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        var timer = Timer(timeInterval: 0.4, target: self, selector:("update"), userInfo: nil, repeats: true)
//        startOtpTimer()
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: 500, height: 500))
        
        scrollView.isUserInteractionEnabled = true
        scrollView.isScrollEnabled = true
        scrollView.backgroundColor = UIColor.white
        scrollView.contentSize = CGSize(width: 500, height: 1000)
        
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.testRefresh(_:)), for: .valueChanged)
        scrollView.addSubview(refreshControl)
        
        view.addSubview(scrollView)
    }
    
//    private func startOtpTimer() {
//        self.totalTime = 7200
//        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
//    }
//    @objc func updateTimer() {
////        print(self.totalTime)
//        print(self.timeFormatted(self.totalTime))
////        self.lblTimer.text = self.timeFormatted(self.totalTime) // will show timer
//        if totalTime != 0 {
//            totalTime -= 1 // decrease counter timer
//        } else {
//            if let timer = self.timer {
//                timer.invalidate()
//                self.timer = nil
//            }
//        }
//    }
//    func timeFormatted(_ totalSeconds: Int) -> String {
//        let hour: Int = totalSeconds / 3600
//        let seconds: Int = totalSeconds % 60
//        let minutes: Int = (totalSeconds / 60) % 60
//
//        return String(format: "%02d:%02d:%02d", hour, minutes, seconds)
//    }
    @objc func testRefresh(_ refreshControl: UIRefreshControl?) {
        print("refresh")
        refreshControl?.endRefreshing()
        
    }
}
