//
//  RegisterViewController.swift
//  ECom
//
//  Created by Nan on 05/04/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var lblName: UITextField!
    @IBOutlet weak var lblEmail: UITextField!
    @IBOutlet weak var lblPassword: UITextField!
    @IBOutlet weak var lblConfirmPassword: UITextField!
    var logEmail = ""
    var logPassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionRegister(sender: UIButton){
        print("Register tapped")
        if(lblName.text!.isEmpty || lblEmail.text!.isEmpty || lblPassword.text!.isEmpty || lblConfirmPassword.text!.isEmpty){
            print("fill all fields")
        }
        else{
            if(lblPassword.text == lblConfirmPassword.text){
                print("register func call")
//                register()
            }
            else{
                print("password not match")
            }
        }
    }
    
    @IBAction func actionLogin(sender: UIButton){
        print("Login tapped")
//        login()
        loginAlertView(msg: "Email")
    }
    
    func register() {
        let details = [
            "name": self.lblName.text as! String,
            "email": self.lblEmail.text as! String,
            "password": self.lblPassword.text as! String] as [String : String]
        Alamofire.upload(multipartFormData: { (multiPartFormData) in
            for (key, value) in details{
                multiPartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: "http://localhost:8080/ECom/register.php" ) { (res) in
            switch res{
            case .success(let upload, _, _):
                upload.uploadProgress(closure:{ (progress) in
                    
                })
                upload.responseString(completionHandler: { (responce) in
                    print(responce)
                })
            case .failure(_):
                print("failure")
            }
        }
    }
    
    func loginAlertView(msg: String) {
        let loginView = UIAlertController(title: "Login", message: msg, preferredStyle: .alert)
        let loginAction = UIAlertAction(title: "LogIn", style: .default) { (action) in
            let txt = loginView.textFields![0] as UITextField
            if(msg == "Email"){
                self.logEmail = txt.text as! String
                print(self.logEmail)
                self.loginAlertView(msg: "Password")
            }
            if(msg == "Password"){
                self.logPassword = txt.text as! String
                print(self.logPassword)
                self.login()
            }
        }
        let cancleAction = UIAlertAction(title: "Cancle", style: .destructive, handler: nil)
        loginView.addTextField { (txtEmail: UITextField) in
            txtEmail.placeholder = "Enter Email"
        }
        loginView.addAction(cancleAction)
        loginView.addAction(loginAction)
        self.present(loginView, animated: true, completion: nil)
    }
    
    func login() {
        var email = ""
        var password = ""
        var status = ""
        Alamofire.request("http://localhost:8080/ECom/login.php").responseJSON { (responce) in
            if let data = responce.result.value as? NSDictionary{
                if let data1 = data["users"] as? NSArray{
                    for i in data1{
                        let dataUsers = i as? NSDictionary
                        email = dataUsers?.value(forKey: "u_email") as! String
                        password = dataUsers?.value(forKey: "u_password") as! String
                        
                        if(self.logEmail == email){
                            if(self.logPassword == password){
                                status = "Success"
                                print("Status\(status)")
                                break;
                            }
                            else{
                                status = "Password not match"
                                print("Status\(status)")
                            }
                        }
                        else{
                            status = "Email not match"
                            print("Status\(status)")
                        }
                    }
                    self.alertView(status: status)
                }
            }
            else {
            }
        }
    }
    
    func alertView(status: String) {
        let alertVC = UIAlertController(title: "Login", message: status, preferredStyle: .alert)
        let successAction = UIAlertAction(title: "Ok", style: .default){ (action) in
            UserDefaults.standard.set("valid", forKey: "log")
            self.redirect(to: "MainScrollViewController")
        }
        let failAction = UIAlertAction(title: "Ok", style: .default){ (action) in
            if(status == "Email not match"){
                self.loginAlertView(msg: "Email")
            }
            else{
                self.loginAlertView(msg: "Password")
            }
        }
        if(status == "Success"){
            alertVC.addAction(successAction)
        }
        else{
            alertVC.addAction(failAction)
        }
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func redirect(to : String){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
