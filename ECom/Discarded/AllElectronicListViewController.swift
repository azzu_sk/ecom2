//
//  AllElectronicListViewController.swift
//  ECom
//
//  Created by Nan on 29/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class AllElectronicListViewController: UIViewController {
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var chgTabBar: UITabBar!
    @IBOutlet weak var home: UITabBarItem!
    @IBOutlet weak var tabSearch: UITabBarItem!
    @IBOutlet weak var wishlist: UITabBarItem!
    @IBOutlet weak var cart: UITabBarItem!
    @IBOutlet weak var account: UITabBarItem!
    var selectedCellIndexPath: IndexPath?
    let selectedCellHeight: CGFloat = 45.0
    let unselectedCellHeight: CGFloat = 50.0
    var tblChk = 1
    var prevIndex: IndexPath?
    
    var items = ["Laptops", "@Mobile Accessories", "Television", "@Audio & Video", "Personal Healthcare Application", "@Cameras & Accessories", "Tablets", "SmartWatch &  Weareble", "@Smart Home Automation", "Printers, Monitors & More", "@Computer Accessories", "Gaming & Accessories", "Desktop PCs", "Mobiles", "Electronic Stores"]

    var childItems = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        chgTabBar.selectedItem = tabSearch
    }
    
    @IBAction func actionBack(_ sender: Any) {
//        print("Back Elec")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController")
        self.present(navController, animated: true, completion: nil)
    }
}
extension AllElectronicListViewController:UITableViewDataSource, UITableViewDelegate, UITabBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if((items[indexPath.row] as String).contains("@")){
            let cell = tableView.dequeueReusableCell(withIdentifier: "AllElectronicCell") as! AllElectronicCell
//            cell.lbladdnremove.isHidden = false
            var txtSplit = (items[indexPath.row] as String).split(separator: "@")
            let showTxt = String(txtSplit[0])
            cell.lblText.text = showTxt
            cell.items = childItems
//            tableView.rowHeight = CGFloat(height)
            cell.tblView.reloadData()
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AllElectronicCell2") as! AllElectronicCell2
            cell.lbl.text = items[indexPath.row]
//            tableView.rowHeight = 45
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row Selected")
        
        func chnageSize(){
            let cell = tableView.cellForRow(at: indexPath) as! AllElectronicCell
            if selectedCellIndexPath != nil && selectedCellIndexPath == indexPath {
                selectedCellIndexPath = nil
                cell.lbladdnremove.text = "+"
//                prevIndex = selectedCellIndexPath
                print("Close")
            } else {
                selectedCellIndexPath = indexPath
                cell.lbladdnremove.text = "-"
//                prevIndex = selectedCellIndexPath
                print("Open")
            }
            
            tableView.beginUpdates()
            tableView.endUpdates()
            
            if selectedCellIndexPath != nil {
                tableView.scrollToRow(at: indexPath, at: .none, animated: true)
            }
        }
        
        if(items[indexPath.row] == items[1]){
            childItems = ["Cases, Covers & Screen gaurd", "Power Banks", "Headphones", "Memory Cards", "On the Go Pen Drives", "Mobile Cables, Selfie Stick & More"]
            chnageSize()
        }
        if(items[indexPath.row] == items[3]){
            childItems = ["Headphones", "Bluethooth Speakers", "Home Theatre Speaker", "Laptop & Decktop Speaker", "Soundbars", "DTH Set Top Box", "Video Player & Recorder", "Audio Player & Recorder"]
            chnageSize()
        }
        if(items[indexPath.row] == items[5]){
            childItems = ["All Cameras", "DSLR", "Sport & Action", "Lens", "All Accessories"]
            chnageSize()
        }
        if(items[indexPath.row] == items[8]){
            childItems = ["Google Home", "Security Camera", "Smart Monitoring Cameras", "Smart Lighting", "Video Door Phone", "Explore All"]
            chnageSize()
        }
        if(items[indexPath.row] == items[10]){
            childItems = ["Explore All", "Hard Disks & Memoy Cards", "Laptops Skins & Decals", "Laptop Bags", "Mouse"]
            chnageSize()
        }
//        cell.items = childItems
//        cell.tblView.reloadData()
        tblView.reloadData()
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if(item == home){
            print("Home")
            let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainScrollViewController")
            self.present(navController, animated: true, completion: nil)
        }
        if(item == tabSearch){
            print("Search")
            let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController")
            self.present(navController, animated: true, completion: nil)
        }
        if(item == wishlist){
            print("WishList")
        }
        if(item == cart){
            print("Cart")
        }
        if(item == account){
            print("Account")
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedCellIndexPath == indexPath {
            return selectedCellHeight * (CGFloat(childItems.count) + 1)
        }
        return unselectedCellHeight
    }
}
