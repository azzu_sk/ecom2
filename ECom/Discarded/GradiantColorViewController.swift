//
//  GradiantColorViewController.swift
//  ECom
//
//  Created by Nan on 05/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class GradiantColorViewController: UIViewController {

    var gradiantlayere = CAGradientLayer()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.orange
        
        gradiantlayere.frame = self.view.bounds
        
        let clr1 = UIColor.yellow.cgColor
        let clr2 = UIColor(red: 1.0, green: 0, blue: 0, alpha: 1.0)
        let clr3 = UIColor.clear.cgColor
        let clr4 = UIColor(white: 0.0, alpha: 1.0)
        
        gradiantlayere.colors = [clr1, clr2, clr3, clr4]
        
        gradiantlayere.locations = [0.0, 0.25, 0.75, 1.0]
        
        self.view.layer.addSublayer(gradiantlayere)
    }
}
