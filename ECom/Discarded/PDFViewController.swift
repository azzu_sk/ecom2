//
//  PDFViewController.swift
//  ECom
//
//  Created by Nan on 04/04/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Alamofire

class PDFViewController: UIViewController {
    
//    let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
    let url = "http://18.216.147.161/uat_p2/uploads/certificates/73%20Westwood%20Road,%20Coventry%20-%20GAS%20-%2013.01.2020%20-%202.pdf"
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        pdfDownload()
    }
    
    
    func pdfDownload() {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("file1.pdf")
            return (documentsURL, [.removePreviousFile])
        }
        
        Alamofire.download(url, to: destination).responseData { response in
            print(response.error.debugDescription)
            if let destinationUrl = response.destinationURL {
                self.alert(msg: "destinationUrl \(destinationUrl.absoluteURL)")
                print("destinationUrl \(destinationUrl.absoluteURL)")
            }
        }
    }
    
    func alert(msg: String) {
        let alertVc = UIAlertController(title: "Done", message: msg, preferredStyle: .alert)
        let alertAc = UIAlertAction(title: "done", style: .default, handler: nil)
        alertVc.addAction(alertAc)
        self.present(alertVc, animated: true, completion: nil)
    }
}
