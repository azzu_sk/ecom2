//
//  ImageUploadViewController.swift
//  ECom
//
//  Created by Nan on 01/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import AssetsLibrary

class ImageUploadViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionPicImg(_sender: Any){
        let imgPicController = UIImagePickerController()
        imgPicController.delegate = self
        imgPicController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imgPicController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imgData = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL{
            ALAssetsLibrary().asset(for: imgData as URL, resultBlock: { (asset) in
                let fileName = asset!.defaultRepresentation()?.filename()
                print(fileName)
                print(imgData)
            }, failureBlock: nil)
        }
        
    }
}
