//
//  allUrl.swift
//  ECom
//
//  Created by Nan on 06/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import Foundation

class allUrl: NSObject {
    //Server links
    var getCatUrl = "https://ecomazzu.000webhostapp.com/ECom/getCategory.php"
    var getProducturl = "https://ecomazzu.000webhostapp.com/ECom/getProduct.php"
    var imgUrlPath = "https://ecomazzu.000webhostapp.com/ECom/uploads/"
    var upDateData = "https://ecomazzu.000webhostapp.com/ECom/updateData.php"
    var loginUrl = "https://ecomazzu.000webhostapp.com/ECom/login.php"
    var registerUrl = "https://ecomazzu.000webhostapp.com/ECom/register.php"
    //Local host links
//    var getCatUrl = "http://localhost:8080/ECom/getCategory.php"
//    var getProducturl = "http://localhost:8080/ECom/getProduct.php"
//    var imgUrlPath = "http://localhost:8080/ECom/uploads/"
//    var upDateData = "http://localhost:8080/ECom/updateData.php"
//    var loginUrl = "http://localhost:8080/ECom/login.php"
//    var registerUrl = "http://localhost:8080/ECom/register.php"
}
