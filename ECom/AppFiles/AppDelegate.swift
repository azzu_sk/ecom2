//
//  FileDATA.filfeegate.swift
//  ECom
//
//  Created by Nan on 24/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SystemConfiguration
import NVActivityIndicatorView

var NVActivityIndicatorView_Message = ""
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var categoryArray = [Category]()
    var productArray = [Product]()
    var cartData = [NSManagedObject]()
    var likeData = [NSManagedObject]()
    var like_p_id = 0
    var cart_Badge = 0
    var cart_p_id = 0
    var rupee = "\u{20B9}"
    var pricefilter : Float!
    var catBool = false
    var proBool = false
    var urlObj = allUrl()
    var sendImg : Data?
    var sendSubImg : Data?
    var queue = DispatchQueue(label: "convImgToData", qos: .userInitiated)
    //    var loadActivity = UIApplication.shared.isNetworkActivityIndicatorVisible
    
    func getCategory() {
        categoryArray.removeAll()
        print("Collecting Category data from server......................................................................")
        //        loadActivity = true
        Alamofire.request(urlObj.getCatUrl).responseJSON { (responce) in
            //            print(responce.result.value as Any)
            if let data = responce.result.value as? NSDictionary{
                if let data1 = data["category"] as? NSArray{
                    self.catBool = true
                    self.queue.async {
                        for loop in data1{
                            let dataCategory = loop as? NSDictionary
                            self.categoryArray.append(Category(cat_id: dataCategory?.value(forKey: "category_id") as? String, cat_name: dataCategory?.value(forKey: "category_name") as? String, cat_icon: dataCategory?.value(forKey: "category_icon") as? String))
                        }
                    }
                }
                if(self.categoryArray.count > 0 && self.productArray.count > 0){
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.changeView(to: "MainScrollViewController")
                }
                else
                {
                    if(self.catBool == false){
                        self.getCategory()
                    }
                    if(self.proBool == false){
                        self.getProduct()
                    }
                }
            }
            else {
                self.catBool = false
            }
        }
    }
    
    func getLike(){
        likeData.removeAll()
        let manObj = persistentContainer.viewContext
        
        let fetchQuery = NSFetchRequest<NSFetchRequestResult>(entityName: "WishList_tb")
        
        do {
            try likeData = manObj.fetch(fetchQuery) as! [NSManagedObject]
        } catch let err as NSError {
            print(err)
        }
        if(likeData.count > 0){
            like_p_id = (likeData[(likeData.count - 1)].value(forKey: "p_id")) as! Int
            //            print(likeData.count)
        }
    }
    
    func getCart(){
        cartData.removeAll()
        let manObj = persistentContainer.viewContext
        
        let fetchQuery = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart_tb")
        
        do {
            try cartData = manObj.fetch(fetchQuery) as! [NSManagedObject]
        } catch let err as NSError {
            print(err)
        }
        if(cartData.count > 0){
            cart_p_id = (cartData[(cartData.count - 1)].value(forKey: "p_id")) as! Int
            cart_Badge = cartData.count
        }
        else{
            cart_Badge = 0
        }
    }
    
    func getProduct()->Void {
        productArray.removeAll()
        print("Collecting Product data from server.......................................................................")
        Alamofire.request(urlObj.getProducturl).responseJSON { (responce) in
            if let data = responce.result.value as? NSDictionary{
                if let data1 = data["product"] as? NSArray{
                    self.proBool = true
                    self.queue.async {
                        for loop in data1{
                            let dataProduct = loop as? NSDictionary
                            if let imgStr = dataProduct?.value(forKey: "product_img") as? String{
                                let imgUrl = URL(string: self.urlObj.imgUrlPath + imgStr)
                                let imgData = try? Data(contentsOf: imgUrl!)
                                if(imgData == nil){
                                    self.sendImg = nil
                                }
                                else{
                                    self.sendImg = imgData
                                }
                            }
                            self.productArray.append(
                                Product.init(productId: dataProduct?.value(forKey: "p_id") as? String,image: dataProduct?.value(forKey: "product_img") as? String, BrandName: dataProduct?.value(forKey: "product_brand_name") as? String, Name: dataProduct?.value(forKey: "product_name") as? String, Price: dataProduct?.value(forKey: "product_price") as? String, Desc: dataProduct?.value(forKey: "product_desc") as? String, CatId: dataProduct?.value(forKey: "product_cat_id") as? String, SubCatId: dataProduct?.value(forKey: "product_sub_cat_id") as? String, SubImg : dataProduct?.value(forKey: "product_sub_img") as? String, proImg : self.sendImg))
                        }
                        DispatchQueue.main.async {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }
                    if(self.catBool && self.proBool){
                        self.changeView(to: "MainScrollViewController")
                    }
                    else{
                        if(self.catBool == false){
                            self.getCategory()
                        }
                        if(self.proBool == false){
                            self.getProduct()
                        }
                    }
                }
            }
            else {
                self.proBool = false
                
            }
        }
    }
    
    func changeView(to : String){
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: to) as UIViewController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = initialViewControlleripad
        self.window?.makeKeyAndVisible()
    }
    
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        getCategory()
        getLike()
        getCart()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //        print(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last as Any);
        //        UIApplication.shared.statusBarStyle = .lightContent
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "ECom")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

