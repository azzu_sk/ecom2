//
//  ClothingViewCell.swift
//  ECom
//
//  Created by Nan on 06/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ClothingViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
}
