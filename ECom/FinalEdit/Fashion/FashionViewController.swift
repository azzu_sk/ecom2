//
//  FashionViewController.swift
//  ECom
//
//  Created by Nan on 06/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class FashionViewController: UIViewController {
    
    @IBOutlet weak var collectionView : UICollectionView!
    var setBanner = ["kidsWearanner.png", "womensWearBanner.png", "mensWearBanner.png"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension FashionViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return setBanner.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FashionViewCell", for: indexPath) as! FashionViewCell
        
        cell.img.image = UIImage(named: setBanner[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row == 0){
            strSubCatId = "1"
            redirect(to: "ClothingViewController")
        }
        if(indexPath.row == 1){
            strSubCatId = "3"
            redirect(to: "ClothingViewController")
        }
        if(indexPath.row == 2){
            strSubCatId = "2"
            redirect(to: "ClothingViewController")
        }
    }
    
    func redirect(to : String){
        backNavigation.append("FashionViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
