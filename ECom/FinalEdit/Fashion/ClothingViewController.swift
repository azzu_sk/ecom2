//
//  ClothingViewController.swift
//  ECom
//
//  Created by Nan on 06/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import CoreData


var strSubCatId = ""
var lblTitleEqualTo = ""
class ClothingViewController: UIViewController {
    
    struct Item {
        var product_id : String!
        var p_id : Int!
        var user_id : String!
        var product_name : String!
        var product_b_name : String!
        var product_price : String!
        var product_img : String!
    }
    struct cartItem {
        var user_id : String!
        var product_id : String!
        var product_name : String!
        var product_b_name : String!
        var product_img : String!
        var product_price : String!
    }
    var itemObj = Item()
    var itemCartObj = cartItem()
    @IBOutlet weak var cartBadgelbl: UILabel!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var lblTitle : UILabel!
    var appDel : AppDelegate!
    var allData = [Product]()
    var productDictionary = [Product]()
    var temArray = [Int]()
    var refreshTimer : Timer?
    let refreshController = UIRefreshControl()
    var p_id = ""
    var like_p_id = 0
    var likeDataCntr = 0
    var imgUrlPath : String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgUrlPath = urlObj.imgUrlPath
        appDel = (UIApplication.shared.delegate as! AppDelegate)
        like_p_id = (appDel.like_p_id)
        refreshController.addTarget(self, action: #selector(self.testRefresh(_:)), for: .valueChanged)
        collectionView.addSubview(refreshController)
        allData = appDel.productArray
        if(appDel.cart_Badge != 0){
            cartBadgelbl.layer.cornerRadius = 7.5
            cartBadgelbl.clipsToBounds = true
            cartBadgelbl.text = String(appDel.cart_Badge)
        }
        else{
            cartBadgelbl.isHidden = true
        }
        if(allData.count > 0){
            for loop in 0...(allData.count - 1){
                let chkCatId = allData[loop].CatId
                let chkSubCatId = allData[loop].SubCatId
                if(chkCatId == proCat_Id && chkSubCatId == strSubCatId){
                    productDictionary.append(allData[loop])
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(productDictionary.count > 0){
            temArrayLoop()
//            cmpArrLoop()
        }
//
//        if(strSubCatId == "1"){
//            lblTitle.text = "Kids Wear"
//        }
//        if(strSubCatId == "2"){
//            lblTitle.text = "Mens Wear"
//        }
//        if(strSubCatId == "3"){
//            lblTitle.text = "Womens Wear"
//        }
        
        lblTitle.text = lblTitleEqualTo
    }
    
    @IBAction func actionBack(_ sender : UIButton){
        redirectBack(to: backNavigation.last!)
    }
    @IBAction func actionHome(_ sender: Any) {
        redirect(to: "MainScrollViewController")
    }
    @IBAction func actionLike(_ sender: Any) {
        redirect(to: "WishListViewController")
    }
    @IBAction func actionCart(_ sender: Any) {
        redirect(to: "CartListViewController")
    }
    
}

extension ClothingViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productDictionary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClothingViewCell", for: indexPath) as! ClothingViewCell
//        cell.img.image =
        cell.likeBtn.tag = indexPath.row
//        let imgUrl = URL(string: (imgUrlPath + productDictionary[indexPath.row].image))
//        let imgData = try? Data(contentsOf: imgUrl!)
//        if(imgData == nil){
//            cell.img.image = UIImage(named: imgUrlPath +  productDictionary[indexPath.row].image)
//        }
//        else{
//            cell.img.image = UIImage(data: imgData!)
//        }
        cell.img.image = UIImage(data: productDictionary[indexPath.row].proImg)
        cell.name.text = productDictionary[indexPath.row].Name
        cell.price.text = productDictionary[indexPath.row].Price + rupee
        
        if(appDel.likeData.count > 0){
            for likeIndex in 0...(appDel.likeData.count - 1){
                let singleObj = appDel.likeData[likeIndex]
                let likeProId = singleObj.value(forKey: "product_id") as! String
                p_id = productDictionary[indexPath.row].productId as String
                if(likeProId == p_id){
                    temArray[indexPath.row] = 1
                }
            }
        }
        
        if(temArray.count > 0){
            if temArray[indexPath.row] == 0 {
                // Write code for default UIButton
                cell.likeBtn.setImage(UIImage(named: "like"), for: .normal)
            }
            else {
                // Write code for What you want to keep UIButton Image Change
                p_id = productDictionary[indexPath.row].productId as String
                cell.likeBtn.setImage(UIImage(named: "Unlike"), for: .normal)
                likeDataCntr += 1
            }
        }
        cell.likeBtn.addTarget(self, action: #selector(likeData), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemControllerProduct_id = productDictionary[indexPath.row].productId
        print(itemControllerProduct_id)
        redirect(to: "ItemDetailViewController")
    }
    
    func temArrayLoop() {
        if(productDictionary.count > 0){
            for _ in 0...(productDictionary.count - 1){
                temArray.append(0)
            }
        }
    }
    
    func redirect(to : String) {
        backNavigation.append("ClothingViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func redirectBack(to : String) {
        backNavigation.removeLast()
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func setLikeItem(){
        let mngObj = appDel.persistentContainer.viewContext
        let insertQuery = NSEntityDescription.insertNewObject(forEntityName: "WishList_tb", into: mngObj)
        insertQuery.setValue(itemObj.p_id, forKey: "p_id")
        insertQuery.setValue(itemObj.product_id, forKey: "product_id")
        insertQuery.setValue(itemObj.user_id, forKey: "user_id")
        insertQuery.setValue(itemObj.product_name, forKey: "product_name")
        insertQuery.setValue(itemObj.product_b_name, forKey: "product_b_name")
        insertQuery.setValue(itemObj.product_price, forKey: "product_price")
        insertQuery.setValue(itemObj.product_img, forKey: "product_img")
        do {
            try mngObj.save()
            appDel.getLike()
        } catch let err as NSError {
            print(err)
        }
    }
    
    func removeWishList(index : Int) {
        let mngObj = appDel.persistentContainer.viewContext
        mngObj.delete(appDel.likeData[index])
        do {
            try mngObj.save()
            appDel.getLike()
        } catch let err as NSError {
            print(err)
        }
    }
    
    func alertController(title:String, message:String, btnName:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: btnName, style: .default, handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func likeData(sender:UIButton){
        if temArray[sender.tag] == 0 {
            //Cart In
            p_id = productDictionary[sender.tag].productId as String
            like_p_id += 1
            itemObj = Item.init(product_id: p_id, p_id: like_p_id, user_id: "1", product_name: productDictionary[sender.tag].Name, product_b_name: productDictionary[sender.tag].BrandName, product_price: productDictionary[sender.tag].Price, product_img: productDictionary[sender.tag].image)
            setLikeItem()
            collectionView.reloadData()
            temArray[sender.tag] = 1
        }
        else {
            //Cart Out
            for likeIndex in 0...(appDel.likeData.count - 1){
                let singleObj = appDel.likeData[likeIndex]
                let likeProId = singleObj.value(forKey: "product_id") as! String
                p_id = productDictionary[sender.tag].productId as String
                if(likeProId == p_id){
                    removeWishList(index: likeIndex)
                    break
                }
            }
            collectionView.reloadData()
            temArray[sender.tag] = 0
        }
    }
    
    @objc func testRefresh(_ refreshControl: UIRefreshControl?) {
        
        if(productDictionary.count == 0){
            appDel.getProduct()
            if(refreshTimer == nil){
                refreshTimer = Timer.scheduledTimer(timeInterval: 05.0, target: self, selector: #selector(testRefresh(_:)), userInfo: nil, repeats: true)
            }
            allData = appDel.productArray
            productDictionary.removeAll()
            if(allData.count > 0){
                for loop in 0...(allData.count - 1){
                    let chkCatId = allData[loop].CatId
                    if(chkCatId == "1"){
                        productDictionary.append(allData[loop])
                    }
                }
                stop()
            }
            print("timer")
        }
        else{
            refreshController.endRefreshing()
            alertController(title: "Data", message: "No Update", btnName: "Ok")
        }
        
        collectionView.reloadData()
    }
    
    @objc func stop(){
        refreshController.endRefreshing()
        refreshTimer?.invalidate()
        refreshTimer = nil
        alertController(title: "Data", message: "Updated", btnName: "Ok")
    }
    
}
