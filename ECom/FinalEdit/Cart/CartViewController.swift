//
//  CartViewController.swift
//  ECom
//
//  Created by Nan on 07/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import CoreData
var chk = 0
class CartViewController: UIViewController {

    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNamr: UILabel!
    @IBOutlet weak var productBrandName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var redClrBtn: UIButton!
    @IBOutlet weak var blackClrBtn: UIButton!
    @IBOutlet weak var goldClrBtn: UIButton!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var btnQtyDec: UIButton!
    @IBOutlet weak var btnQtyInc: UIButton!
    @IBOutlet weak var collectonSimilarProduct: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    var product_id : String!
    var p_id : Int!
    var user_id : String!
    var product_name : String!
    var product_b_name : String!
    var product_price : String!
    var product_clr : String!
    var product_qty : String!
    var product_img : String!
    var appdel:AppDelegate!
    var collData = [Product]()
    var cartProduct : Product?
    var product = [Product]()
    var imgUrlPath : String!
    var qtyCntr = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imgUrlPath = urlObj.imgUrlPath
        scrollView.isHidden = true
        appdel = UIApplication.shared.delegate as? AppDelegate
        p_id = appdel.cart_p_id
        lblQty.text = String(qtyCntr)
        product_clr = "NA"
        product = appdel.productArray
        DispatchQueue.main.async {
            self.activityView.startAnimating()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setValues()
    }
    
    @IBAction func actionRedClr(_ sender: Any) {
        product_clr = "Red"
        redClrBtn.layer.borderWidth = 3
        redClrBtn.layer.borderColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        blackClrBtn.layer.borderWidth = 0
        goldClrBtn.layer.borderWidth = 0
    }
    
    @IBAction func actionBlackClr(_ sender: Any) {
        product_clr = "Black"
        blackClrBtn.layer.borderWidth = 3
        blackClrBtn.layer.borderColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        redClrBtn.layer.borderWidth = 0
        goldClrBtn.layer.borderWidth = 0
    }
    
    @IBAction func actionGoldClr(_ sender: Any) {
        product_clr = "Gold"
        goldClrBtn.layer.borderWidth = 3
        goldClrBtn.layer.borderColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        redClrBtn.layer.borderWidth = 0
        blackClrBtn.layer.borderWidth = 0
    }
    
    @IBAction func actionQtyDec(_ sender: Any) {
        if(qtyCntr > 1){
            qtyCntr -= 1
            lblQty.text = String(qtyCntr)
        }
    }
    
    @IBAction func actionQtyInc(_ sender: Any) {
        qtyCntr += 1
        lblQty.text = String(qtyCntr)
    }
    
    @IBAction func actionAddToCart(_ sender: Any) {
       setCartItem()
    }
    
    @IBAction func actionNBack(_ sender: Any) {
        redirectBack(to: backNavigation.last!)
    }
    
    @IBAction func actionDetail(_ sender: Any) {
        itemControllerProduct_id = product_id
        redirect(to: "ItemDetailViewController")
    }
    
    @IBAction func actionHome(_ sender: Any) {
        redirect(to: "MainScrollViewController")
    }
}

extension CartViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CartCollectionViewCell", for: indexPath) as! CartCollectionViewCell
        cell.img.image = UIImage(data: collData[indexPath.row].proImg)
        cell.lbl.text = collData[indexPath.row].Name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemControllerProduct_id = collData[indexPath.row].productId
        redirect(to: "ItemDetailViewController")
    }
    
    func setCartItem(){
        p_id += 1
        let mngObj = appdel.persistentContainer.viewContext
        
        let insertQuery = NSEntityDescription.insertNewObject(forEntityName: "Cart_tb", into: mngObj)
        let cart_product_price = Int(product_price)!
        insertQuery.setValue(p_id, forKey: "p_id")
        insertQuery.setValue(product_id, forKey: "product_id")
        insertQuery.setValue(user_id, forKey: "user_id")
        insertQuery.setValue(product_b_name, forKey: "product_brand")
        insertQuery.setValue(product_clr, forKey: "product_color")
        insertQuery.setValue(product_name, forKey: "product_name")
        insertQuery.setValue(qtyCntr, forKey: "product_qty")
        insertQuery.setValue(cart_product_price, forKey: "cart_product_price")
        insertQuery.setValue(product_img, forKey: "product_img")
        do {
            try mngObj.save()
            appdel.getCart()
            let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CartListViewController")
            self.present(navController, animated: true, completion: nil)
        } catch let err as NSError {
            print(err)
        }
    }
    func redirect(to : String){
        backNavigation.append("CartViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func redirectBack(to : String){
        backNavigation.removeLast()
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func setValues() {
        for loop in 0...(product.count - 1){
            if(product[loop].productId == itemControllerProduct_id){
                cartProduct = product[loop]
            }
        }
        collectonSimilarProduct.reloadData()
        for loop in 0...(product.count - 1){
            if(product[loop].SubCatId == strSubCatId && product[loop].CatId == proCat_Id){
                collData.append(product[loop])
            }
        }
        productNamr.text = cartProduct?.Name
        productBrandName.text = cartProduct?.BrandName
        productPrice.text = (cartProduct?.Price)! + rupee
        let imgUrl = URL(string: (imgUrlPath + (cartProduct?.image)!))
        let imgData = try? Data(contentsOf: imgUrl!)
        if(imgData == nil){
            productImg.image = UIImage(named: imgUrlPath + (cartProduct?.image)!)
        }
        else{
            productImg.image = UIImage(data: imgData!)
        }
        product_price = cartProduct?.Price
        product_id = cartProduct?.productId
        product_b_name = cartProduct?.BrandName
        product_name = cartProduct?.Name
        product_img = cartProduct?.image
        
        DispatchQueue.main.async {
            self.activityView.stopAnimating()
            self.activityView.isHidden = true
            self.scrollView.isHidden = false
        }
    }
    
}
