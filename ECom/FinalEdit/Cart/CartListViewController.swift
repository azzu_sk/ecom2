//
//  CartListViewController.swift
//  ECom
//
//  Created by Nan on 08/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import CoreData
import BraintreeDropIn
import Braintree

class CartListViewController: UIViewController {
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var emptyCartView: UIView!
    @IBOutlet weak var editBtn: UIButton!
    
    var controllerName : String!
    var appDel : AppDelegate!
    var product = [NSManagedObject]()
    var totalAmount = 0
    var prevQty = 0
    var upQty = [Int]()
    var imgUrlPath : String!
    let url = "https://sandbox.braintreegateway.com/merchants/nmrd229jdvx8qvrv/users/j9rybjzdy2kyxkrs/api_keys"
    override func viewDidLoad() {
        super.viewDidLoad()
        imgUrlPath = urlObj.imgUrlPath
        emptyCartView.isHidden = true
        appDel = UIApplication.shared.delegate as? AppDelegate
        editBtn.tag = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        DispatchQueue.main.async {
            self.activityView.startAnimating()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        product = appDel.cartData
        check()
    }
    
    @IBAction func actionContinueShopping(_ sender: Any) {
        redirect(to: "MainScrollViewController")
    }
    
    @IBAction func actionHome(_ sender: Any) {
        redirect(to: "MainScrollViewController")
    }
    
    @IBAction func actionBack(_ sender: Any) {
        redirectBack(to: backNavigation.last!)
    }
    
    @IBAction func actionCheckOut(_ sender: Any) {
        showDropIn(clientTokenOrTokenizationKey: "sandbox_7byxxrbk_nmrd229jdvx8qvrv")
    }
    
    @IBAction func actionEdit(_ sender: UIButton) {
        if(sender.tag == 0){
            tblView.isEditing = true
            editBtn.setImage(#imageLiteral(resourceName: "cancelW"), for: .normal)
            sender.tag = 1
        }else{
            tblView.isEditing = false
            editBtn.setImage(#imageLiteral(resourceName: "edit"), for: .normal)
            sender.tag = 0
            
        }
    }
}

extension CartListViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return product.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let singleObj = product[indexPath.row]
        let img = singleObj.value(forKey: "product_img") as! String
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartListTableViewCell") as! CartListTableViewCell
        let imgUrl = URL(string: (imgUrlPath + img))
        let imgData = try? Data(contentsOf: imgUrl!)
        if(imgData == nil){
            cell.productImg.image = UIImage(named: imgUrlPath + img)
        }
        else{
            cell.productImg.image = UIImage(data: imgData!)
        }
        let price = singleObj.value(forKey: "cart_product_price") as! NSNumber
        cell.lblProductName.text = singleObj.value(forKey: "product_name") as? String
        cell.lblBrandName.text = singleObj.value(forKey: "product_brand") as? String
        cell.productPrice.text = appDel.rupee + String(Int(truncating: price) * upQty[indexPath.row])
        cell.lblQty.text = String(upQty[indexPath.row])
        totalAmount = totalAmount + (Int(truncating: price) * upQty[indexPath.row])
        lblAmount.text = appDel.rupee + String(totalAmount)
        cell.cartAdd.tag = indexPath.row
        cell.cartLess.tag = indexPath.row
        cell.cartAdd.addTarget(self, action: #selector(incQty), for: .touchUpInside)
        cell.cartLess.addTarget(self, action: #selector(decQty), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 151.0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        print(indexPath.row)
        removeCartList(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cartProId = appDel.cartData[indexPath.row]
        itemControllerProduct_id = cartProId.value(forKey: "product_id") as! String
        redirect(to: "ItemDetailViewController")
    }
    
    func redirect(to : String) {
        backNavigation.append("CartListViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func redirectBack(to : String) {
        backNavigation.removeLast()
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    
    func removeCartList(index : Int) {
        let mngObj = appDel.persistentContainer.viewContext
        
        mngObj.delete(appDel.cartData[index])
        do {
            try mngObj.save()
            appDel.getCart()
            product = appDel.cartData
            totalAmount = 0
            lblAmount.text = String(totalAmount)
            upQty.removeAll()
            tblView.reloadData()
            check()
        } catch let err as NSError {
            print(err)
        }
    }
    
    @objc func incQty(_sender : UIButton){
        let myIndexPath = NSIndexPath(row: _sender.tag, section: 0)
        _ = tblView.cellForRow(at: myIndexPath as IndexPath) as! CartListTableViewCell
        prevQty = upQty[_sender.tag]
        prevQty += 1
        upQty[_sender.tag] = prevQty
        totalAmount = 0
        tblView.reloadData()
    }
    
    @objc func decQty(_sender : UIButton){
        if(upQty[_sender.tag] > 1){
            let myIndexPath = NSIndexPath(row: _sender.tag, section: 0)
            _ = tblView.cellForRow(at: myIndexPath as IndexPath) as! CartListTableViewCell
            prevQty = upQty[_sender.tag]
            prevQty -= 1
            upQty[_sender.tag] = prevQty
            totalAmount = 0
            tblView.reloadData()
        }
    }
    
    func check() {
        if(product.count > 0){
            tblView.isHidden = true
            for singleIndex in 0...(product.count - 1){
                let singleObj = product[singleIndex]
                let qty = singleObj.value(forKey: "product_qty") as! NSNumber
                upQty.append(Int(truncating: qty))
            }
            tblView.isHidden = false
            emptyCartView.isHidden = true
            tblView.reloadData()
        }
        else{
            editBtn.setImage(#imageLiteral(resourceName: "edit"), for: .normal)
            tblView.isHidden = true
            editBtn.isEnabled = false
            emptyCartView.isHidden = false
        }
        DispatchQueue.main.async {
            self.activityView.stopAnimating()
            self.activityView.isHidden = true
        }
    }
    
    func activityStart() {
        DispatchQueue.main.async {
        }
       
    }
    
    func fetchClientToken() {
        // TODO: Switch this URL to your own authenticated API
        let clientTokenURL = NSURL(string: url)!
        let clientTokenRequest = NSMutableURLRequest(url: clientTokenURL as URL)
        clientTokenRequest.setValue("text/plain", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: clientTokenRequest as URLRequest) { (data, response, error) -> Void in
            // TODO: Handle errors
            //            print("data\(data)")
            print("responce\(String(describing: response))")
            //            print("error\(error)")
            let clientToken = String(data: data!, encoding: String.Encoding.utf8)
            print("clientToken\(String(describing: clientToken))")
            // As an example, you may wish to present Drop-in at this point.
            //            self.showDropIn(clientTokenOrTokenizationKey: (clientToken)!)
            // Continue to the next section to learn more...
            }.resume()
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                print("result : \(String(describing: result.paymentMethod))")
                self.postNonceToServer(paymentMethodNonce: result.paymentDescription)
                // Use the BTDropInResult properties to update your UI
                // result.paymentOptionType
                // result.paymentMethod
                // result.paymentIcon
                // result.paymentDescription
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
    
    func postNonceToServer(paymentMethodNonce: String) {
        // Update URL with your server
        let paymentURL = URL(string: "http://localhost:8080/paypal/pay.php/fake-valid-nonce")!
        var request = URLRequest(url: paymentURL)
        request.httpBody = "payment_method_nonce=\(paymentMethodNonce)".data(using: String.Encoding.utf8)
        request.httpMethod = "POST"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
            // TODO: Handle success or failure
            print("responce : \(String(describing: response))")
            }.resume()
    }
}
