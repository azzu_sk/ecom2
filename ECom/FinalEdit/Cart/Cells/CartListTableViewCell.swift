//
//  CartListTableViewCell.swift
//  ECom
//
//  Created by Nan on 08/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class CartListTableViewCell: UITableViewCell {

    @IBOutlet weak var cartLess: UIButton!
    @IBOutlet weak var cartAdd: UIButton!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
