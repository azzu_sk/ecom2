//
//  CartCollectionViewCell.swift
//  ECom
//
//  Created by Nan on 07/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class CartCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img : UIImageView!
    @IBOutlet weak var lbl : UILabel!
}
