//
//  o5_CollectionCell.swift
//  ECom
//
//  Created by Nan on 28/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o5_CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl:UILabel!
    @IBOutlet weak var img:UIImageView!
    
}
