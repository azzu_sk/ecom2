//
//  o12_CollectionCell.swift
//  ECom
//
//  Created by Nan on 28/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o12_CollectionCell: UICollectionViewCell {
    @IBOutlet weak var img:UIImageView!
    @IBOutlet weak var lblBold: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblOffer: UILabel!
}
