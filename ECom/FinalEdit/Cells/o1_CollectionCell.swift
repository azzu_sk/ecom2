//
//  o1_CollectionCell.swift
//  ECom
//
//  Created by Nan on 25/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o1_CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
}
