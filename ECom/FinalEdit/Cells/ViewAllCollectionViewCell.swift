//
//  ViewAllCollectionViewCell.swift
//  ECom
//
//  Created by Nan on 15/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ViewAllCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellImg: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDesc: UILabel!
    
}
