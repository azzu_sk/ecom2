//
//  o14_CollectionCell.swift
//  ECom
//
//  Created by Nan on 28/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o14_CollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblProduct:UILabel!
    @IBOutlet weak var lblSponcer:UILabel!
    @IBOutlet weak var img:UIImageView!
}
