//
//  SignInandSignUpViewController.swift
//  ECom
//
//  Created by Nan on 01/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Alamofire

class SignInandSignUpViewController: UIViewController {
    
    @IBOutlet weak var lbl:UILabel!
    @IBOutlet weak var txtEmailorNum:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    var logEmail = ""
    var logPassword = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lbl.text = "Be in sync With all\nyour Device, Orders\nand Wishlist"
    }
    
    @IBAction func actionSignIn(_sender:Any){
        print("actionSignIn")
        logEmail = txtEmailorNum.text as! String
        logPassword = txtPassword.text as! String
        login()
    }
    
    @IBAction func actionSignUp(_sender:Any){
        print("actionSignUp")
        redirect(to: "SignUpViewController")
    }
    
    @IBAction func actionClosePage(_sender:Any){
        print("actionClosePage")
        
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AccSignOutViewController")
        self.present(navController, animated: true, completion: nil)
    }
    
    func alertView(status: String) {
        let alertVC = UIAlertController(title: "Login", message: status, preferredStyle: .alert)
        let successAction = UIAlertAction(title: "Ok", style: .default){ (action) in
            UserDefaults.standard.set("yes", forKey: "active")
            self.redirect(to: "MainScrollViewController")
        }
        let failAction = UIAlertAction(title: "Ok", style: .default){ (action) in
//            if(status == "Email not match"){
//                self.loginAlertView(msg: "Email")
//            }
//            else{
//                self.loginAlertView(msg: "Password")
//            }
        }
        if(status == "Success"){
            alertVC.addAction(successAction)
        }
        else{
            alertVC.addAction(failAction)
        }
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func redirect(to : String){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
//    func loginAlertView(msg: String) {
//        let loginView = UIAlertController(title: "Login", message: msg, preferredStyle: .alert)
//        let loginAction = UIAlertAction(title: "LogIn", style: .default) { (action) in
//            let txt = loginView.textFields![0] as UITextField
//            if(msg == "Email"){
//                self.logEmail = txt.text as! String
//                print(self.logEmail)
//                self.loginAlertView(msg: "Password")
//            }
//            if(msg == "Password"){
//                self.logPassword = txt.text as! String
//                print(self.logPassword)
//                self.login()
//            }
//        }
//        let cancleAction = UIAlertAction(title: "Cancle", style: .destructive, handler: nil)
//        loginView.addTextField { (txtEmail: UITextField) in
//            txtEmail.placeholder = "Enter Email"
//        }
//        loginView.addAction(cancleAction)
//        loginView.addAction(loginAction)
//        self.present(loginView, animated: true, completion: nil)
//    }
    
    func login() {
        var email = ""
        var password = ""
        var status = ""
        Alamofire.request(urlObj.loginUrl).responseJSON { (responce) in
            if let data = responce.result.value as? NSDictionary{
                if let data1 = data["users"] as? NSArray{
                    for i in data1{
                        let dataUsers = i as? NSDictionary
                        email = dataUsers?.value(forKey: "u_email") as! String
                        password = dataUsers?.value(forKey: "u_password") as! String
                        
                        if(self.logEmail == email){
                            if(self.logPassword == password){
                                status = "Success"
                                print("Status\(status)")
                                break;
                            }
                            else{
                                status = "Password not match"
                                print("Status\(status)")
                                break;
                            }
                        }
                        else{
                            status = "Email not match"
                            print("Status\(status)")
                        }
                    }
                    self.alertView(status: status)
                }
            }
            else {
            }
        }
    }
    
}
