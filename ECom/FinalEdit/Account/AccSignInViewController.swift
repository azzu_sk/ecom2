//
//  AccSignInViewController.swift
//  ECom
//
//  Created by Nan on 01/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class AccSignInViewController: UIViewController {
    
    @IBOutlet weak var chgTabBar: UITabBar!
    @IBOutlet weak var home: UITabBarItem!
    @IBOutlet weak var tabSearch: UITabBarItem!
    @IBOutlet weak var wishlist: UITabBarItem!
    @IBOutlet weak var cart: UITabBarItem!
    @IBOutlet weak var account: UITabBarItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        chgTabBar.selectedItem = account
    }
    @IBAction func actionSignOut(_ sender: Any) {
        UserDefaults.standard.set("no", forKey: "active")
        redirect(to: "MainScrollViewController")
    }
}

extension AccSignInViewController: UITabBarDelegate{
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item == home){
            redirect(to: "MainScrollViewController")
        }
        if(item == tabSearch){
            redirect(to: "MenuViewController")
        }
        if(item == wishlist){
            redirect(to: "WishListViewController")
        }
        if(item == cart){
            redirect(to: "CartListViewController")
        }
        if(item == account){
            //
        }
    }
    
    func redirect(to : String){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
