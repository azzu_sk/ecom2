//
//  SignUpViewController.swift
//  ECom
//
//  Created by Nan on 07/04/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Alamofire

class SignUpViewController: UIViewController {

    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    var chkValid: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblMsg.text = "Your Data is safe\nwith us. We won't\nshare it with anyone"
    }
    @IBAction func actionClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func actionSignup(_ sender: Any) {
        if(txtFullName.text!.isEmpty || txtEmail.text!.isEmpty || lblPassword.text!.isEmpty || txtConfirmPassword.text!.isEmpty){
            print("fill all fields")
        }
        else{
            if(lblPassword.text == txtConfirmPassword.text){
                print("register func call")
                register()
            }
            else{
                print("password not match")
            }
        }
    }
    @IBAction func actionExistingUser(_ sender: Any) {
        redirect(to: "SignInandSignUpViewController")
    }
    
    func redirect(to : String){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func register() {
        let details = [
            "name": self.txtFullName.text as! String,
            "email": self.txtEmail.text as! String,
            "password": self.lblPassword.text as! String] as [String : String]
        Alamofire.upload(multipartFormData: { (multiPartFormData) in
            for (key, value) in details{
                multiPartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: urlObj.registerUrl ) { (res) in
            switch res{
            case .success(let upload, _, _):
                upload.uploadProgress(closure:{ (progress) in
                    
                })
                upload.responseString(completionHandler: { (responce) in
//                    print(responce.value)
                    var str = responce.value as NSString?
                    if let jsonStr = str as? String{
                        let statusData = jsonStr.data(using: String.Encoding.utf8)
                        do{
                            let jsonData = try JSONSerialization.jsonObject(with: statusData!, options: JSONSerialization.ReadingOptions.mutableContainers)
                            let status = jsonData as! NSDictionary
                            print(status.value(forKey: "Status"))
                            if(status.value(forKey: "Status") as! String == "success"){
                                    self.alertView(msg: self.txtFullName.text!)
                            }
                        }catch{
                           print(responce.error.debugDescription)
                        }
                    }
                })
            case .failure(_):
                print("failure")
            }
        }
        
    }
    
    func alertView(msg: String) {
        let alertVC = UIAlertController(title: "SignedIn", message: "Wellcome \(msg)", preferredStyle: .alert)
        let alertAC = UIAlertAction(title: "OK", style: .default) { (action) in
            UserDefaults.standard.set("yes", forKey: "active")
            self.redirect(to: "MainScrollViewController")
        }
        alertVC.addAction(alertAC)
        self.present(alertVC, animated: true, completion: nil)
    }
}
