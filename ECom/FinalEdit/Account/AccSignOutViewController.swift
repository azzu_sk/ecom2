//
//  AccSignOutViewController.swift
//  ECom
//
//  Created by Nan on 01/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class AccSignOutViewController: UIViewController {
    @IBOutlet weak var chgTabBar: UITabBar!
    @IBOutlet weak var home: UITabBarItem!
    @IBOutlet weak var tabSearch: UITabBarItem!
    @IBOutlet weak var wishlist: UITabBarItem!
    @IBOutlet weak var cart: UITabBarItem!
    @IBOutlet weak var account: UITabBarItem!
    var controllerName : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        chgTabBar.selectedItem = account
    }
    
    @IBAction func actionSignInSignUp(_ sender: Any) {
        redirect(to: "SignInandSignUpViewController")
    }
}

extension AccSignOutViewController: UITabBarDelegate{
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item == home){
            redirect(to: "MainScrollViewController")
        }
        if(item == tabSearch){
            redirect(to: "MenuViewController")
        }
        if(item == wishlist){
            redirect(to: "WishListViewController")
        }
        if(item == cart){
            redirect(to: "CartListViewController")
        }
        if(item == account){
          //
        }
    }
    
    func redirect(to : String){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
