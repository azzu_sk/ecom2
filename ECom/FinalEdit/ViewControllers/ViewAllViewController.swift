//
//  ViewAllViewController.swift
//  ECom
//
//  Created by Nan on 15/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ViewAllViewController: UIViewController {
    
    var appDel:AppDelegate!
    var productDictionary = [Product]()
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appDel = UIApplication.shared.delegate as? AppDelegate
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        productDictionary = appDel.productArray
    }
    
    @IBAction func actionBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension ViewAllViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productDictionary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewAllCollectionViewCell", for: indexPath) as! ViewAllCollectionViewCell
        cell.cellImg.image = UIImage(data: productDictionary[indexPath.row].proImg)
        cell.productName.text = productDictionary[indexPath.row].Name
        cell.productDesc.text = productDictionary[indexPath.row].Desc
        return cell
    }
    
    func redirect(to : String){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
