//
//  ItemDetailViewController.swift
//  ECom
//
//  Created by Nan on 14/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Cosmos

var itemControllerProduct_id = ""
let rupee = "\u{20B9}"
var urlObj = allUrl()
class ItemDetailViewController: UIViewController {

    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var subImgBtn: UIButton!
    @IBOutlet weak var subImgBtn1: UIButton!
    @IBOutlet weak var subImgBtn2: UIButton!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var rateStar: CosmosView!
    @IBOutlet weak var collectionProductView: UICollectionView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var timeStart : Timer!
    var product = [Product]()
    var tblProduct = [Product]()
    var appdel:AppDelegate!
    var imgArr = [String]()
    var mainQueue = DispatchQueue.main
    var queue = DispatchQueue(label: "loadData", qos: .userInitiated)
    var imgUrlPath : String!
    var btnImgUrlPath : String!
    var cellSelect = false
    override func viewDidLoad() {
        super.viewDidLoad()
        imgUrlPath = urlObj.imgUrlPath
        btnImgUrlPath = urlObj.imgUrlPath
        appdel = UIApplication.shared.delegate as? AppDelegate
        product = appdel!.productArray
        activityView.isHidden = true
        scrollView.isHidden = true
        rateStar.settings.fillMode = .precise
        rateStar.didFinishTouchingCosmos = { (rating) in
            self.rateButton.setTitle(String(round(rating)), for: .normal)
        }
        loadStart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(product.count > 0){
            setValues()
        }
    }
    
    @IBAction func actionHome(_ sender: Any) {
        redirect(to: "MainScrollViewController")
    }
    
    @IBAction func actionFullView(_ sender: Any) {
        if(subImgBtn.isEnabled){
            ProductImgView_Id = itemControllerProduct_id
            redirect(to: "ProductImgViewController")
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        product.removeAll()
        redirectBack(to: backNavigation.last!)
    }
    
    func redirect(to : String){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func redirectBack(to : String){
        backNavigation.removeLast()
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc func setValues() {
        tblProduct.removeAll()
        imgArr.removeAll()
        lblBrandName.text = "BrandName : "
        productNameLbl.text = "Name : "
        lblPrice.text = "Price : "
        for loop in 0...(product.count - 1){
            if(product[loop].SubCatId == strSubCatId && product[loop].CatId == proCat_Id){
                tblProduct.append(product[loop])
            }
            if(product[loop].productId == itemControllerProduct_id){
                let subImgs = product[loop].SubImg.split(separator: ",")
                if(subImgs.count > 0){
                    for subLoop in 0...(subImgs.count - 1){
                        imgArr.append(String(subImgs[subLoop]))
                    }
                }
                if(imgArr.count > 0){
                    for btnLoop in 0...2{
                        let btnImgUrl = URL(string: (btnImgUrlPath + imgArr[btnLoop]))
                        let btnImgData = try? Data(contentsOf: btnImgUrl!)
                        if(btnImgData == nil){
                            if(btnLoop == 0){
                                subImgBtn.setImage(UIImage(named: btnImgUrlPath + imgArr[btnLoop]), for: .normal)
                            }
                            if(btnLoop == 1){
                                subImgBtn1.setImage(UIImage(named: btnImgUrlPath + imgArr[btnLoop]), for: .normal)
                            }
                            if(btnLoop == 2){
                                subImgBtn2.setImage(UIImage(named: btnImgUrlPath + imgArr[btnLoop]), for: .normal)
                            }
                        }
                        else{
                            if(btnLoop == 0){
                                subImgBtn.setImage(UIImage(data: btnImgData!), for: .normal)
                            }
                            if(btnLoop == 1){
                                subImgBtn1.setImage(UIImage(data: btnImgData!), for: .normal)
                            }
                            if(btnLoop == 2){
                                subImgBtn2.setImage(UIImage(data: btnImgData!), for: .normal)
                            }
                        }
                    }
                }
                else{
                    subImgBtn.setImage(UIImage(named: "noIMG"), for: .normal)
                    subImgBtn1.setImage(UIImage(named: "noIMG"), for: .normal)
                    subImgBtn2.setImage(UIImage(named: "noIMG"), for: .normal)
                    subImgBtn.isEnabled = false
                    subImgBtn1.isEnabled = false
                    subImgBtn2.isEnabled = false
                }
                productImg.image = UIImage(data: product[loop].proImg)
                lblProductName.text = product[loop].Name
                productNameLbl.text = productNameLbl.text! + product[loop].Name
                lblBrandName.text = lblBrandName.text! + product[loop].BrandName
                lblPrice.text = lblPrice.text! + product[loop].Price + rupee
            }
        }
        if(timeStart != nil){
            timeStart.invalidate()
        }
        loadFinish()
    }
    
    @IBAction func subImgOne(_ sender: UIButton) {
        UIView.transition(with: productImg, duration: 0.75, options: .transitionCrossDissolve, animations: { self.productImg.image = sender.currentImage }, completion: nil)
    }
    
    @IBAction func subImgTwo(_ sender: UIButton) {
        UIView.transition(with: productImg, duration: 0.75, options: .transitionCrossDissolve, animations: { self.productImg.image = sender.currentImage }, completion: nil)
    }
    
    @IBAction func subImgThree(_ sender: UIButton) {
        UIView.transition(with: productImg, duration: 0.75, options: .transitionCrossDissolve, animations: { self.productImg.image = sender.currentImage }, completion: nil)
    }
    
    @IBAction func actionAddToCart(_ sender: Any) {
        chk = 1
        redirect(to: "CartViewController")
    }
}

extension ItemDetailViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tblProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemDetailCell", for: indexPath) as! ItemDetailCell
        if(tblProduct[indexPath.row].SubCatId == strSubCatId && tblProduct[indexPath.row].CatId == proCat_Id){
            cell.similarProductImg.image = UIImage(data: tblProduct[indexPath.row].proImg)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemControllerProduct_id = tblProduct[indexPath.row].productId
        proCat_Id = tblProduct[indexPath.row].CatId
        cellSelect = true
        DispatchQueue.main.async{
            self.scrollView.isHidden = true
            self.loadStart()
            self.timeStart = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.setValues), userInfo: nil, repeats: true)
        }
    }
    
    func loadStart() {
        activityView.isHidden = false
        activityView.startAnimating()
    }
    
    func loadFinish() {
        activityView.isHidden = true
        scrollView.isHidden = false
        collectionProductView.reloadData()
        activityView.stopAnimating()
        scrollView.contentOffset = CGPoint(x: 0, y: -scrollView.contentInset.top)
    }

}
