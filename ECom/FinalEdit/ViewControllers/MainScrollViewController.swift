//
//  MainScrollViewController.swift
//  ECom
//
//  Created by Nan on 25/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import SystemConfiguration

var backNavigation = [String]()
class MainScrollViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet  var scrollView: UIScrollView!
    @IBOutlet weak var chgTabBar: UITabBar!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var home: UITabBarItem!
    @IBOutlet weak var search: UITabBarItem!
    @IBOutlet weak var wishlist: UITabBarItem!
    @IBOutlet weak var cart: UITabBarItem!
    @IBOutlet weak var account: UITabBarItem!
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var refreshBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchProductTable: UIView!
    
    var img = ["img1", "img2", "img3"]
    var timer: Timer?
    var totalTime = 60
    var appDel:AppDelegate!
    var controllerName : String!
    var refreshTimer : Timer?
    var productDictonary = [Product]()
    var searchContainer: o16_containerViewController?
//    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appDel = UIApplication.shared.delegate as? AppDelegate
//        UserDefaults.standard.set("no", forKey: "active")
        activityLoader.isHidden = true
        refreshBtn.layer.cornerRadius = 14
        refreshBtn.clipsToBounds = true
        refreshBtn.isHidden = true
        startDealOfDayTimer()
        searchBtn.tag = 0
//        searchBar.backgroundImage = UIImage()
        searchBar.isHidden = true
        searchProductTable.isHidden = true
//        startTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(appDel.cart_Badge != 0){
            cart.badgeValue = String(appDel.cart_Badge)
        }
        chgTabBar.selectedItem = home
    }
    
    
//    override func viewDidAppear(_ animated: Bool) {
//        if isConnectedToNetwork(){
//        }else{
//            offlineAlert()
//        }
//    }

    
    @IBAction func actionSearch(_ sender: UIButton) {
        
        if(sender.tag == 0){
            searchProductTable.isHidden = false
            UIView.transition(with: sender, duration: 0.4, options: .transitionFlipFromRight, animations: {
                self.searchBtn.setImage(#imageLiteral(resourceName: "cancelW"), for: .normal)
            }, completion: nil)
//            searchBtn.setImage(#imageLiteral(resourceName: "cancelW"), for: .normal)
            searchBar.alpha = 0
            searchBar.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.searchBar.alpha = 1
            }
            sender.tag = 1
            searchContainer?.tableView.reloadData()
            searchContainer?.productDictionary = appDel.productArray
        }
        else{
            searchProductTable.isHidden = true
            UIView.transition(with: sender, duration: 0.4, options: .transitionFlipFromRight, animations: {
                self.searchBtn.setImage(#imageLiteral(resourceName: "searchW"), for: .normal)
            }, completion: nil)
//            searchBtn.setImage(#imageLiteral(resourceName: "searchW"), for: .normal)
            UIView.animate(withDuration: 0.3, animations: {
                self.searchBar.alpha = 0
            }) { (finished) in
                self.searchBar.isHidden = finished
            }
            sender.tag = 0
        }
    }
    @IBAction func actionAdminPannel(_ sender: Any) {
        redirect(to: "AddProductViewController")
    }
    @IBAction func viewAllDealsOfDay(_ sender: Any) {
        redirect(to: "ViewAllViewController")
    }
    @IBAction func viewAllDiscount4U(_ sender: Any) {
        redirect(to: "ViewAllViewController")
    }
    @IBAction func viewAllNew(_ sender: Any) {
        redirect(to: "ViewAllViewController")
    }
    @IBAction func viewAllSales(_ sender: Any) {
        redirect(to: "ViewAllViewController")
    }
    @IBAction func actionRefreshPage(_ sender: Any) {
        refreshBtn.isHidden = true
//        activ()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "o16_containerViewController"{
            searchContainer = segue.destination as! o16_containerViewController
            searchContainer!.delegate = self
        }
    }
    
}


extension MainScrollViewController:UICollectionViewDelegate,UICollectionViewDataSource, UIScrollViewDelegate, UITabBarDelegate, UISearchBarDelegate, o16_containerProtocol{
//    func searchKey(txt: String) {
//        //
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "o0_CollectionCell", for: indexPath) as! o0_CollectionCell
        cell.img.image = UIImage(named: img[indexPath.row])
        return cell
    }
    
//    func startTimer() {
//        _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAuto), userInfo: nil, repeats: true)
//        _ = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.showRefreshBtn), userInfo: nil, repeats: true)
//    }
    
//    @objc func showRefreshBtn(){
//        refreshBtn.isHidden = false
//    }
    
    @objc func scrollAuto(){
        if let col = collection{
            for cell in col.visibleCells{
                let indexPath:IndexPath? = col.indexPath(for: cell)
                if((indexPath?.row)! < 2){
                    let indexPATH1:IndexPath?
                    indexPATH1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    col.scrollToItem(at: indexPATH1!, at: .right, animated: true)
                }
                else{
                    let indexPATH1:IndexPath?
                    indexPATH1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    col.scrollToItem(at: indexPATH1!, at: .left, animated: true)
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
            chgTabBar.isHidden = false
//            if(activityLoader.isHidden == false ){
//                activityLoader.isHidden = false
//            }
//            if(refreshBtn.isHidden == false){
//                 refreshBtn.isHidden = false
//            }
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        chgTabBar.isHidden = true
        searchBar.resignFirstResponder()
//        refreshBtn.isHidden = true
//        activityLoader.isHidden = true
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if(item == home){
            print("MainScrollViewController")
        }
        if(item == search){
            controllerName = "MenuViewController"
            redirect(to: controllerName)
        }
        if(item == wishlist){
            controllerName = "WishListViewController"
            redirect(to: controllerName)
        }
        if(item == cart){
            controllerName = "CartListViewController"
            redirect(to: controllerName)
        }
        if(item == account){
            if(UserDefaults.standard.string(forKey: "active") == "yes"){
                controllerName = "AccSignInViewController"
            }
            else{
                controllerName = "AccSignOutViewController"
            }
             redirect(to: controllerName)
        }
    }
    
    private func startDealOfDayTimer() {
        self.totalTime = 7200
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.lblTimer.text = self.timeFormatted(self.totalTime) // will show timer
        if totalTime != 0 {
            totalTime -= 1 // decrease counter timer
        } else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
            }
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let hour: Int = totalSeconds / 3600
//        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02dh%02dm", hour, minutes)
//        return String(format: "%02d:%02d:%02d", hour, minutes, seconds)
    }
    
    func redirect(to : String){
        backNavigation.append("MainScrollViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }

    /*
    func offlineAlert() {
        let alertController = UIAlertController(title: "Connection Status", message: "Offline", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            self.chkConnection()
        }
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func activ(){
        DispatchQueue.main.async {
            self.activityLoader.startAnimating()
            self.activityLoader.isHidden = false
            self.appDel.refreshBool = false
            self.appDel.getProduct()
            self.appDel.getCategory()
//            self.data.getProduct()
//            self.data.getCategory()
            self.stopRefresh()
        }
    }
    
    @objc func stopRefresh(){
        if(refreshTimer == nil){
            refreshTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.stopRefresh), userInfo: nil, repeats: true)
        }
    }
    
    func stop() {
        DispatchQueue.main.async {
            if(self.appDel.refreshBool){
                self.activityLoader.stopAnimating()
                //                                self.tblView.isHidden = false
                self.activityLoader.isHidden = true
                //                                self.tblView.reloadData()
                if self.refreshTimer != nil {
                    self.refreshTimer!.invalidate()
                    self.refreshTimer = nil
                }
            }
//            else{
//                self.stop()
//            }
        }
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    @objc func chkConnection() {
        if isConnectedToNetwork(){
            print("Internet Connection Available!")
            refreshTimer?.invalidate()
        }else{
            offlineAlert()
            print("Internet Connection not Available!")
            refreshTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(chkConnection), userInfo: nil, repeats: true)
            chkConnection()
        }
    }
    */
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        productDictonary = appDel.productArray
        let filteredArr = productDictonary.filter{ lbl in
            return lbl.Name.lowercased().contains(searchText.lowercased()) }
        if(filteredArr.count > 0){
            productDictonary = filteredArr
            searchContainer?.filterProduct(product: filteredArr)
        }
        else{
            productDictonary = appDel.productArray
        }
    }
}

