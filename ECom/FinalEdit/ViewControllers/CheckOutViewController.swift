//
//  CheckOutViewController.swift
//  ECom
//
//  Created by Nan on 22/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import BraintreeDropIn
import Braintree

class CheckOutViewController: UIViewController {

    let url = "https://sandbox.braintreegateway.com/merchants/nmrd229jdvx8qvrv/users/j9rybjzdy2kyxkrs/api_keys"
    //    let url = "https://braintree-sample-merchant.herokuapp.com/client_token"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionPay(_ sender: Any) {
        showDropIn(clientTokenOrTokenizationKey: "sandbox_7byxxrbk_nmrd229jdvx8qvrv")
    }
    
    func fetchClientToken() {
        let clientTokenURL = NSURL(string: url)!
        let clientTokenRequest = NSMutableURLRequest(url: clientTokenURL as URL)
        clientTokenRequest.setValue("text/plain", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: clientTokenRequest as URLRequest) { (data, response, error) -> Void in
            // TODO: Handle errors
//            let clientToken = String(data: data!, encoding: String.Encoding.utf8)
            // As an example, you may wish to present Drop-in at this point.
            //            self.showDropIn(clientTokenOrTokenizationKey: (clientToken)!)
            // Continue to the next section to learn more...
            }.resume()
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                self.postNonceToServer(paymentMethodNonce: result.paymentDescription)
                // Use the BTDropInResult properties to update your UI
                // result.paymentOptionType
                // result.paymentMethod
                // result.paymentIcon
                // result.paymentDescription
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
    
    func postNonceToServer(paymentMethodNonce: String) {
        // Update URL with your server
        let paymentURL = URL(string: "http://localhost:8080/paypal/pay.php/fake-valid-nonce")!
        var request = URLRequest(url: paymentURL)
        request.httpBody = "payment_method_nonce=\(paymentMethodNonce)".data(using: String.Encoding.utf8)
        request.httpMethod = "POST"
        URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
            // TODO: Handle success or failure
            }.resume()
    }
}
