//
//  CompareViewController.swift
//  ECom
//
//  Created by Nan on 15/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

var compareViewProductArr = [String]()
class CompareViewController: UIViewController {
    
    @IBOutlet weak var productImg1: UIImageView!
    @IBOutlet weak var productImg2: UIImageView!
    @IBOutlet weak var lblName1: UILabel!
    @IBOutlet weak var lblName2: UILabel!
    @IBOutlet weak var tblView: UITableView!
    var appDel:AppDelegate!
    var productDictionary = [Product]()
    var productOne = Product()
    var productTwo = Product()
    var tblRowCount = 1
    var imgUrlPath : String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgUrlPath = urlObj.imgUrlPath
        appDel = UIApplication.shared.delegate as? AppDelegate
        productDictionary = appDel.productArray
        productImg1.layer.borderWidth = 0.5
        productImg2.layer.borderWidth = 0.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(productDictionary.count > 0){
            for loop in 0...(productDictionary.count - 1){
                if(productDictionary[loop].productId == compareViewProductArr[0]){
                    productOne = productDictionary[loop]
                }
                if(productDictionary[loop].productId == compareViewProductArr[1]){
                    productTwo = productDictionary[loop]
                }
            }
        }
        let oneImgUrl = URL(string: (imgUrlPath + productOne.image))
        let oneImgData = try? Data(contentsOf: oneImgUrl!)
        if(oneImgData == nil){
            productImg1.image = UIImage(named: imgUrlPath + productOne.image)
        }
        else{
            productImg1.image = UIImage(data: oneImgData!)
        }
        let twoImgUrl = URL(string: (imgUrlPath + productTwo.image))
        let twoImgData = try? Data(contentsOf: twoImgUrl!)
        if(twoImgData == nil){
            productImg2.image = UIImage(named: imgUrlPath + productTwo.image)
        }
        else{
            productImg2.image = UIImage(data: twoImgData!)
        }
        lblName1.text = productOne.Name
        lblName2.text = productTwo.Name
    }
    
    @IBAction func actionClose(_ sender: Any) {
        compareViewProductArr.removeAll()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionProductOne(_ sender: Any) {
        itemControllerProduct_id = productOne.productId
        redirect(to: "ItemDetailViewController")
    }
    
    @IBAction func actionProductTwo(_ sender: Any) {
        itemControllerProduct_id = productTwo.productId
        redirect(to: "ItemDetailViewController")
    }
    
}
extension CompareViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblRowCount
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.layer.borderWidth = 0.5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompareTableViewCell") as! CompareTableViewCell
        cell.lblTitle.text = "Price"
        cell.lblPro1Val.text = productOne.Price + rupee
        cell.lblPro1Val2.text = productTwo.Price + rupee
        return cell
    }
    
    func redirect(to : String){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
