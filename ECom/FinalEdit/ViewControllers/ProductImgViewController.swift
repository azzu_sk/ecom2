//
//  ProductImgViewController.swift
//  ECom
//
//  Created by Nan on 14/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

var ProductImgView_Id = ""
class ProductImgViewController: UIViewController {

    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var pageController: UIPageControl!
    var cntr = 0
    var appDel:AppDelegate!
    var img = [Product]()
    var imgArr = [String]()
    var imgUrlPath : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imgUrlPath = urlObj.imgUrlPath
        appDel = (UIApplication.shared.delegate as! AppDelegate)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getData()  
    }
    
    @IBAction func actionClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            if(cntr < (imgArr.count - 1 )){
                cntr += 1
                pageController.currentPage = cntr
                let imgUrl = URL(string: (imgUrlPath + imgArr[cntr]))
                let imgData = try? Data(contentsOf: imgUrl!)
                if(imgData == nil){
                    productImg.image = UIImage(named: imgUrlPath +  imgArr[cntr])
                }
                else{
                    productImg.image = UIImage(data: imgData!)
                }
            }
        }
        if sender.direction == .right {
            if(cntr > 0){
                cntr -= 1
                pageController.currentPage = cntr
                let imgUrl = URL(string: (imgUrlPath + imgArr[cntr]))
                let imgData = try? Data(contentsOf: imgUrl!)
                if(imgData == nil){
                    productImg.image = UIImage(named: imgUrlPath +  imgArr[cntr])
                }
                else{
                    productImg.image = UIImage(data: imgData!)
                }
            }
        }
    }
    
    func getData() {
        img = appDel.productArray
        for loop in 0...(img.count - 1){
            if(img[loop].productId == ProductImgView_Id){
                let subImgs = img[loop].SubImg.split(separator: ",")
                if(subImgs.count > 0){
                    for subLoop in 0...(subImgs.count - 1){
                        imgArr.append(String(subImgs[subLoop]))
                    }
                }
            }
        }
        if(imgArr.count > 0){
            let imgUrl = URL(string: (imgUrlPath + imgArr[cntr]))
            let imgData = try? Data(contentsOf: imgUrl!)
            if(imgData == nil){
                productImg.image = UIImage(named: imgUrlPath +  imgArr[cntr])
            }
            else{
                productImg.image = UIImage(data: imgData!)
            }
            pageController.numberOfPages = imgArr.count
        }
    }
    
    func redirect(to : String){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
