//
//  MenuViewController.swift
//  ECom
//
//  Created by Nan on 28/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var chgTabBar: UITabBar!
    @IBOutlet weak var home: UITabBarItem!
    @IBOutlet weak var tabSearch: UITabBarItem!
    @IBOutlet weak var wishlist: UITabBarItem!
    @IBOutlet weak var cart: UITabBarItem!
    @IBOutlet weak var account: UITabBarItem!
    var lbl = [String]()
    var img = [String]()
    var catItem = [Category]()
    var appDel:AppDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        appDel = UIApplication.shared.delegate as? AppDelegate
        img = ["elec", "tv", "fashionMenu", "homeNferniture", "toys", "beauty", "sports", "game", "gift"]
        catItem = appDel.categoryArray
        search.backgroundImage = UIImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        chgTabBar.selectedItem = tabSearch
    }
}
extension MenuViewController:UITableViewDelegate, UITableViewDataSource, UITabBarDelegate, UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if(lbl.count == 0){
//            if(appDel.category.isEmpty == false){
//                lbl = appDel.category
//            }
//            else{
//                lbl = ["Electronic","Tvs & Application", "Fashion", "Home & Furniture", "Toys & Baby", "Beauty & Personal care", "Sports, Books & More", "Game Zone", "Gift Card"]
//            }
//        }
//        if(img.count == 0){
//            img = ["elec", "tv", "fashion", "homeNferniture", "toys", "beauty", "sports", "game", "gift"]
//        }
        return catItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTblCell") as! MenuTblCell
//        cell.img.image = UIImage(named: img[indexPath.row])
        cell.lbl.text = catItem[indexPath.row].cat_name
        cell.img.image = UIImage (named: catItem[indexPath.row].cat_icon)
//        if(cell.lbl.text == "Electronic"){
//            cell.img.image = UIImage(named: img[0])
//        }
//        if(cell.lbl.text == "Tvs & Application"){
//            cell.img.image = UIImage(named: img[1])
//        }
//        if(cell.lbl.text == "Fashion"){
//            cell.img.image = UIImage(named: img[2])
//        }
//        if(cell.lbl.text == "Home & Furniture"){
//            cell.img.image = UIImage(named: img[3])
//        }
//        if(cell.lbl.text == "Toys & Baby"){
//            cell.img.image = UIImage(named: img[4])
//        }
//        if(cell.lbl.text == "Beauty & Personal care"){
//            cell.img.image = UIImage(named: img[5])
//        }
//        if(cell.lbl.text == "Sports, Books & More"){
//            cell.img.image = UIImage(named: img[6])
//        }
//        if(cell.lbl.text == "Game Zone"){
//            cell.img.image = UIImage(named: img[7])
//        }
//        if(cell.lbl.text == "Gift Card"){
//            cell.img.image = UIImage(named: img[8])
//        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(catItem[indexPath.row].cat_name == "Electronic"){
            redirect(to: "ElectronicExpViewController")
        }
        tblView.reloadData()
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if(item == home){
            redirect(to: "MainScrollViewController")
        }
        if(item == search){
            //
        }
        if(item == wishlist){
            redirect(to: "WishListViewController")
        }
        if(item == cart){
            redirect(to: "CartListViewController")
        }
        if(item == account){
            if(UserDefaults.standard.string(forKey: "active") == "yes"){
                 redirect(to: "AccSignInViewController")
            }
            else{
                 redirect(to: "AccSignOutViewController")
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        search.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        catItem = appDel.categoryArray
        let filteredArr = catItem.filter{ lbl in
            return lbl.cat_name.lowercased().contains(searchText.lowercased()) }
        if(filteredArr.count > 0){
            catItem = filteredArr
        }
        else{
           catItem = appDel.categoryArray
        }
        tblView.reloadData()
        print(filteredArr)
    }
    
    func redirect(to : String){
        backNavigation.append("MenuViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
