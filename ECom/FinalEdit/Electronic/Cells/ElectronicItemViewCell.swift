//
//  ElectronicItemViewCell.swift
//  ECom
//
//  Created by Nan on 01/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ElectronicItemViewCell: UITableViewCell {
    
    @IBOutlet weak var img:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblBrandName:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var addTocartBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var cmprBtn: UIButton!
    var cntr = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
