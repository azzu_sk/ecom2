//
//  ExpItemsTableViewCell.swift
//  ECom
//
//  Created by Nan on 31/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ExpItemsTableViewCell: UITableViewCell {

    static var itemId = "ExpItemsTableViewCell"
    @IBOutlet weak var itemLbl:UILabel!
}
class NoemalExpCells: UITableViewCell {
    @IBOutlet weak var normalCells:UILabel!
    static var normalId = "NoemalExpCells"
}

class ExpandableExpCell: ExpandedCell {
    static var expId = "ExpandableExpCell"
    @IBOutlet weak var expandingCell:UILabel!
    
}
