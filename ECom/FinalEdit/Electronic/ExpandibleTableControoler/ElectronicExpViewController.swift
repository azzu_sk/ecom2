//
//  ElectronicExpViewController.swift
//  ECom
//
//  Created by Nan on 31/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import ExpandableCell

class ElectronicExpViewController: UIViewController {

    @IBOutlet weak var tblView : ExpandableTableView!
    @IBOutlet weak var chgTabBar: UITabBar!
    @IBOutlet weak var home: UITabBarItem!
    @IBOutlet weak var tabSearch: UITabBarItem!
    @IBOutlet weak var wishlist: UITabBarItem!
    @IBOutlet weak var cart: UITabBarItem!
    @IBOutlet weak var account: UITabBarItem!
    
    var childItems = [""]
    
     var items = ["Laptops", "@Mobile Accessories", "Television", "@Audio & Video", "Personal Healthcare Application", "@Cameras & Accessories", "Tablets", "SmartWatch &  Weareble", "@Smart Home Automation", "Printers, Monitors & More", "@Computer Accessories", "Gaming & Accessories", "Desktop PCs", "Mobiles", "Electronic Stores"]
    var cellArr = [UITableViewCell]()
    var controllerName : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        chgTabBar.selectedItem = tabSearch
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tblView.expandableDelegate = self
        tblView.animation = .automatic
        tblView.register(UINib(nibName: "ExpItemsTableViewCell", bundle: nil), forCellReuseIdentifier: ExpItemsTableViewCell.itemId)
        tblView.register(UINib(nibName: "NoemalExpCells", bundle: nil), forCellReuseIdentifier: NoemalExpCells.normalId)
        tblView.register(UINib(nibName: "ExpandableExpCell", bundle: nil), forCellReuseIdentifier: ExpandableExpCell.expId)
    }
    @IBAction func actionBack(_ sender: Any) {
        redirectBack(to: backNavigation.last!)
    }
    @IBAction func actionHome(_ sender: Any) {
        redirectBack(to: "MainScrollViewController")
    }
    
}
extension ElectronicExpViewController:ExpandableDelegate, UITabBarDelegate{
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
        return [30,30,30,30,30,30,30,30]
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
        switch indexPath.row {
        case 1:
            cellArr.removeAll()
            childItems = ["Cases, Covers & Screen gaurd", "Power Banks", "Headphones", "Memory Cards", "On the Go Pen Drives", "Mobile Cables, Selfie Stick & More"]
            for index in 0...(childItems.count - 1){
                let cell = tblView.dequeueReusableCell(withIdentifier: ExpItemsTableViewCell.itemId) as! ExpItemsTableViewCell
                cell.itemLbl.text = childItems[index]
                cellArr.append(cell)
            }
            return cellArr
        case 3:
            cellArr.removeAll()
            childItems = ["Headphones", "Bluethooth Speakers", "Home Theatre Speaker", "Laptop & Decktop Speaker", "Soundbars", "DTH Set Top Box", "Video Player & Recorder", "Audio Player & Recorder"]
            for index in 0...(childItems.count - 1){
                let cell = tblView.dequeueReusableCell(withIdentifier: ExpItemsTableViewCell.itemId) as! ExpItemsTableViewCell
                cell.itemLbl.text = childItems[index]
                cellArr.append(cell)
            }
            return cellArr
        case 5:
            cellArr.removeAll()
            childItems = ["All Cameras", "DSLR", "Sport & Action", "Lens", "All Accessories"]
            for index in 0...(childItems.count - 1){
                let cell = tblView.dequeueReusableCell(withIdentifier: ExpItemsTableViewCell.itemId) as! ExpItemsTableViewCell
                cell.itemLbl.text = childItems[index]
                cellArr.append(cell)
            }
            return cellArr
        case 8:
            cellArr.removeAll()
            childItems = ["Google Home", "Security Camera", "Smart Monitoring Cameras", "Smart Lighting", "Video Door Phone", "Explore All"]
            for index in 0...(childItems.count - 1){
                let cell = tblView.dequeueReusableCell(withIdentifier: ExpItemsTableViewCell.itemId) as! ExpItemsTableViewCell
                cell.itemLbl.text = childItems[index]
                cellArr.append(cell)
            }
            return cellArr
        case 10:
            cellArr.removeAll()
            childItems = ["Explore All", "Hard Disks & Memoy Cards", "Laptops Skins & Decals", "Laptop Bags", "Mouse"]
            for index in 0...(childItems.count - 1){
                let cell = tblView.dequeueReusableCell(withIdentifier: ExpItemsTableViewCell.itemId) as! ExpItemsTableViewCell
                cell.itemLbl.text = childItems[index]
                cellArr.append(cell)
            }
            return cellArr
        default:
            break
        }
        return nil
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if((items[indexPath.row] as String).contains("@")){
            let cell = expandableTableView.dequeueReusableCell(withIdentifier: "ExpandableExpCell") as! ExpandableExpCell
            var txtSplit = (items[indexPath.row] as String).split(separator: "@")
            let showTxt = String(txtSplit[0])
            cell.expandingCell.text = showTxt
            return cell
        }
        else{
            let cell = expandableTableView.dequeueReusableCell(withIdentifier: "NoemalExpCells") as! NoemalExpCells
            cell.normalCells.text = items[indexPath.row]
            return cell
        }
       
    }
    
    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tblView.cellForRow(at: indexPath) as? NoemalExpCells{
            let cellLbl = cell.normalCells.text
            if(cellLbl == "Mobiles"){
                proCat_Id = "1"
                strSubCatId = "1"
                lblTitleEqualTo = "Mobiles"
                redirect(to: "ElectronicItemViewController")
            }
        }
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
        if let cell = expandedCell as? ExpItemsTableViewCell {
            print("\(cell.itemLbl.text ?? "")")
        }
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if(item == home){
            redirect(to: "MainScrollViewController")
        }
        if(item == tabSearch){
            redirect(to: "MenuViewController")
        }
        if(item == wishlist){
            redirect(to: "WishListViewController")
        }
        if(item == cart){
            redirect(to: "CartListViewController")
        }
        if(item == account){
            if(UserDefaults.standard.string(forKey: "active") == "yes"){
                controllerName = "AccSignInViewController"
            }
            else{
                controllerName = "AccSignOutViewController"
            }
            redirect(to: controllerName)
        }
    }
    func redirect(to : String){
        backNavigation.append("ElectronicExpViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func redirectBack(to : String){
        backNavigation.removeLast()
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
