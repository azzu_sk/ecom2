//
//  ElectronicItemViewController.swift
//  ECom
//
//  Created by Nan on 28/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import CoreData

var proCat_Id = ""

class ElectronicItemViewController: UIViewController {
    @IBOutlet weak var lblBtnBadge: UILabel!
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblAlertHeader: UILabel!
    @IBOutlet weak var lblPriceFilter: UILabel!
    @IBOutlet weak var alertSlider: UISlider!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var lowToHighBtn: UIButton!
    @IBOutlet weak var highToLowBtn: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var btnCompare: UIButton!
    @IBOutlet weak var btnSubmitCompare: UIButton!
    var cmpBtnBool = false
    var sortVar = 0
    var appDel:AppDelegate!
    var allData = [Product]()
    var productDictionary = [Product]()
    var imgUrlPath : String!
    var indexNum = [IndexPath]()
    var temArray = [Int]()
    var p_id = ""
    var like_p_id = 0
    var likeDataCntr = 0
    var filteredProduct = [Product]()
    var filterFlag = 0
    var sortFlag = 0
    var cmpArr = [Int]()
    var cmpCntr = 0
    var sendCmpArr = [String]()
    struct Item {
        var product_id : String!
        var p_id : Int!
        var user_id : String!
        var product_name : String!
        var product_b_name : String!
        var product_price : String!
        var product_img : String!
    }
    struct cartItem {
        var user_id : String!
        var product_id : String!
        var product_name : String!
        var product_b_name : String!
        var product_img : String!
        var product_price : String!
    }
    var itemObj = Item()
    var itemCartObj = cartItem()
    var controllerName : String?
    var refreshTimer : Timer?
    let refreshController = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imgUrlPath = urlObj.imgUrlPath
        appDel = (UIApplication.shared.delegate as! AppDelegate)
        like_p_id = appDel.like_p_id
        alertView.isHidden = true
        sortView.isHidden = true
        lowToHighBtn.tag = 0
        highToLowBtn.tag = 1
        allData = appDel.productArray
        lblTitle.text = lblTitleEqualTo
        appDel.pricefilter = nil
        chk = 0
        if(appDel.pricefilter != nil){
            lblPriceFilter.text = " > : " + String(appDel.pricefilter)
        }
        if(appDel.cart_Badge != 0){
            lblBtnBadge.layer.cornerRadius = 7.5
            lblBtnBadge.clipsToBounds = true
            lblBtnBadge.text = String(appDel.cart_Badge)
        }
        else{
            lblBtnBadge.isHidden = true
        }
        btnSubmitCompare.isHidden = true
        refreshController.isAccessibilityElement = true
        refreshController.addTarget(self, action: #selector(self.testRefresh(_:)), for: .valueChanged)
        tblView.addSubview(refreshController)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(allData.count > 0){
            getData()
        }
        else{
            tblView.isHidden = true
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        redirectBack(to: backNavigation.last!)
    }
    @IBAction func actionHome(_ sender: Any) {
        redirect(to: "MainScrollViewController")
    }
    @IBAction func actionWishList(_ sender: Any) {
        redirect(to: "WishListViewController")
    }
    @IBAction func actionCart(_ sender: Any) {
        redirect(to: "CartListViewController")
    }
    @IBAction func actionCompare(_ sender: UIButton) {
        print("actionCompare")
        if(sender.tag == 0){
            btnSubmitCompare.isHidden = false
            cmpBtnBool = true
            tblView.reloadData()
            sender.tag = 1
        }
        else{
            btnSubmitCompare.isHidden = true
            cmpBtnBool = false
            tblView.reloadData()
            sender.tag = 0
        }
    }
    @IBAction func actionSort(_ sender: UIButton) {
        if(sender.tag == 0){
            alertView.isHidden = true
            btnFilter.tag = 0
            sortView.alpha = 0
            sortView.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.sortView.alpha = 1
            }
            sender.tag = 1
        }
        else{
            UIView.animate(withDuration: 0.3, animations: {
                self.sortView.alpha = 0
            }) { (finished) in
                self.sortView.isHidden = finished
            }
            sender.tag = 0
        }
    }
    
    @IBAction func actionFilter(_ sender: UIButton) {
        if(appDel.pricefilter != nil){
            lblPriceFilter.text = ">" + String(appDel.pricefilter)
        }
        else{
            lblPriceFilter.text = "Default"
        }
        if(sender.tag == 0){
            sortView.isHidden = true
            btnSort.tag = 0
            alertView.alpha = 0
            alertView.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.alertView.alpha = 1
            }
            sender.tag = 1
        }
        else{
            UIView.animate(withDuration: 0.3, animations: {
                self.alertView.alpha = 0
            }) { (finished) in
                self.alertView.isHidden = finished
            }
            sender.tag = 0
        }
    }
    
    @IBAction func actionApply(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alertView.alpha = 0
        }) { (finished) in
            self.alertView.isHidden = finished
        }
        appDel.pricefilter = round(alertSlider.value)
        filterData(less: Int(alertSlider.value), flag: true)
    }
    
    @IBAction func actionCancle(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alertView.alpha = 0
        }) { (finished) in
            self.alertView.isHidden = finished
        }
        appDel.pricefilter = nil
        filterData(less: 0, flag: false)
        alertSlider.setValue(alertSlider.minimumValue, animated: true)
    }
    
    @IBAction func actionAlertSlider(_ sender: Any) {
        alertSlider.setValue(round(alertSlider.value/500) * 500, animated: true)
        lblPriceFilter.text = ">" + String(round(alertSlider.value))
    }
    
    @IBAction func actionSortCancle(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.sortView.alpha = 0
        }) { (finished) in
            self.sortView.isHidden = finished
        }
        sortFlag = 1
        sortedData(_sender: lowToHighBtn)
        lowToHighBtn.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
        highToLowBtn.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
    }
    
    @IBAction func actionSortApply(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.sortView.alpha = 0
        }) { (finished) in
            self.sortView.isHidden = finished
        }
        sortFlag = 0
        if(lowToHighBtn.isSelected){
            sortedData(_sender: lowToHighBtn)
        }
        if(highToLowBtn.isSelected){
            sortedData(_sender: highToLowBtn)
        }
    }
    
    @IBAction func actionL2H(_ sender: UIButton) {
        lowToHighBtn.isSelected = true
        highToLowBtn.isSelected = false
        lowToHighBtn.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        highToLowBtn.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
    }
    @IBAction func actionH2L(_ sender: UIButton) {
        lowToHighBtn.isSelected = false
        highToLowBtn.isSelected = true
        highToLowBtn.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        lowToHighBtn.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
    }
   
    @IBAction func actionSubmitCompare(_ sender: Any) {
        cmpCntr = 0
        for loop in 0...(cmpArr.count - 1){
            if(cmpArr[loop] == 1){
                cmpCntr += 1
            }
        }
        if(cmpCntr < 2){
            alertController(title: "Oops", message: "Atleast 2 items", btnName: "Ok")
        }
        if(cmpCntr == 2){
            let one = (sendCmpArr.count - 2)
            let two = (sendCmpArr.count - 1)
            compareViewProductArr.append(sendCmpArr[one])
            compareViewProductArr.append(sendCmpArr[two])
            redirect(to: "CompareViewController")
        }
    }
}

extension ElectronicItemViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productDictionary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ElectronicItemViewCell") as! ElectronicItemViewCell
        cell.addTocartBtn.tag = indexPath.row
        cell.likeBtn.tag = indexPath.row
        cell.cmprBtn.tag = indexPath.row
        cell.lblName.text = productDictionary[indexPath.row].Name
        cell.lblBrandName.text = productDictionary[indexPath.row].BrandName
        cell.lblPrice.text = productDictionary[indexPath.row].Price + rupee
        cell.img.image = UIImage(data: productDictionary[indexPath.row].proImg)
        
//        if(imgUrlPath != nil){
//            let imgUrl = URL(string: (imgUrlPath + productDictionary[indexPath.row].image))
//            let imgData = try? Data(contentsOf: imgUrl!)
//            if(imgData == nil){
//                cell.img.image = UIImage(named: imgUrlPath +  productDictionary[indexPath.row].image)
//            }
//            else{
//                cell.img.image = UIImage(data: imgData!)
//            }
//        }
//        else{
//            cell.img.image = UIImage(named:  productDictionary[indexPath.row].image)
//        }
        
        if(appDel.likeData.count > 0){
            for likeIndex in 0...(appDel.likeData.count - 1){
                let singleObj = appDel.likeData[likeIndex]
                let likeProId = singleObj.value(forKey: "product_id") as! String
                p_id = productDictionary[indexPath.row].productId as String
                if(likeProId == p_id){
                    temArray[indexPath.row] = 1
                }
            }
        }
        if(cmpBtnBool){
            cell.cmprBtn.isHidden = false
            if(cmpArr.count > 0){
                if(cmpArr[indexPath.row] == 0){
                    cell.cmprBtn.backgroundColor = #colorLiteral(red: 0.4352941215, green: 0.4431372583, blue: 0.4745098054, alpha: 1)
                }
                else{
                    cell.cmprBtn.backgroundColor = #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1)
                }
            }
        }
        else{
            cell.cmprBtn.isHidden = true
        }
        if(temArray.count > 0){
            if temArray[indexPath.row] == 0 {
                // Write code for default UIButton
                cell.likeBtn.setImage(UIImage(named: "like"), for: .normal)
            }
            else {
                // Write code for What you want to keep UIButton Image Change
                p_id = productDictionary[indexPath.row].productId as String
                cell.likeBtn.setImage(UIImage(named: "Unlike"), for: .normal)
                likeDataCntr += 1
            }
        }
        cell.likeBtn.addTarget(self, action: #selector(likeData), for: .touchUpInside)
//        cell.addTocartBtn.addTarget(self, action: #selector(cartData), for: .touchUpInside)
        cell.cmprBtn.addTarget(self, action: #selector(cmpProduct), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        itemControllerProduct_id = productDictionary[indexPath.row].productId
        strSubCatId = productDictionary[indexPath.row].SubCatId
        redirect(to: "ItemDetailViewController")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    @objc func likeData(sender:UIButton){
//        UIButton.animate(withDuration: 0.2,animations:{ sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)},completion: { finish in UIButton.animate(withDuration: 0.2, animations: { sender.transform = CGAffineTransform.identity })})
        UIView.animate(withDuration: 0.2, animations: { sender.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },completion: { _ in UIView.animate(withDuration: 0.2) { sender.transform = CGAffineTransform.identity }
        })
        if temArray[sender.tag] == 0 {
            //Cart In
            p_id = productDictionary[sender.tag].productId as String
            like_p_id += 1
            itemObj = Item.init(product_id: p_id, p_id: like_p_id, user_id: "1", product_name: productDictionary[sender.tag].Name, product_b_name: productDictionary[sender.tag].BrandName, product_price: productDictionary[sender.tag].Price, product_img: productDictionary[sender.tag].image)
            setLikeItem()
            tblView.reloadData()
            temArray[sender.tag] = 1
        }
        else {
            //Cart Out
            for likeIndex in 0...(appDel.likeData.count - 1){
                let singleObj = appDel.likeData[likeIndex]
                let likeProId = singleObj.value(forKey: "product_id") as! String
                p_id = productDictionary[sender.tag].productId as String
                if(likeProId == p_id){
                    removeWishList(index: likeIndex)
                    break
                }
            }
            tblView.reloadData()
            temArray[sender.tag] = 0
        }
    }
    
//    @objc func cartData(sender:UIButton){
////        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
////        let cell = tblView.cellForRow(at: myIndexPath as IndexPath)
//            //Cart In
//        p_id = productDictionary[sender.tag].productId as String
//        itemCartObj = cartItem(user_id: "1", product_id: p_id, product_name: productDictionary[sender.tag].Name, product_b_name: productDictionary[sender.tag].BrandName, product_img: productDictionary[sender.tag].image, product_price: productDictionary[sender.tag].Price)
////        print(itemCartObj.product_img)
//        self.performSegue(withIdentifier: "CartViewController", sender: self)
//    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if(segue.identifier == "CartViewController"){
//            let cartClsObj = segue.destination as! CartViewController
//            cartClsObj.product_id = itemCartObj.product_id
//            cartClsObj.user_id = itemCartObj.user_id
//            cartClsObj.product_name = itemCartObj.product_name
//            cartClsObj.product_b_name = itemCartObj.product_b_name
//            cartClsObj.product_img = itemCartObj.product_img
//            cartClsObj.product_price = itemCartObj.product_price
//        }
//    }
    
    func setLikeItem(){
        let mngObj = appDel.persistentContainer.viewContext
        
        let insertQuery = NSEntityDescription.insertNewObject(forEntityName: "WishList_tb", into: mngObj)
        insertQuery.setValue(itemObj.p_id, forKey: "p_id")
        insertQuery.setValue(itemObj.product_id, forKey: "product_id")
        insertQuery.setValue(itemObj.user_id, forKey: "user_id")
        insertQuery.setValue(itemObj.product_name, forKey: "product_name")
        insertQuery.setValue(itemObj.product_b_name, forKey: "product_b_name")
        insertQuery.setValue(itemObj.product_price, forKey: "product_price")
        insertQuery.setValue(itemObj.product_img, forKey: "product_img")
        do {
            try mngObj.save()
            appDel.getLike()
        } catch let err as NSError {
            print(err)
        }
    }
    
    func removeWishList(index : Int) {
        let mngObj = appDel.persistentContainer.viewContext
        
        mngObj.delete(appDel.likeData[index])
        do {
            try mngObj.save()
            appDel.getLike()
        } catch let err as NSError {
            print(err)
        }
    }
    func redirect(to : String) {
        backNavigation.append("ElectronicItemViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    func redirectBack(to : String) {
        backNavigation.removeLast()
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func filterData(less : Int, flag : Bool) {
        if(flag){
            filteredProduct.removeAll()
            if(appDel.productArray.count > 0){
                for arrIndex in 0...(productDictionary.count - 1){
                    let singleObj = productDictionary[arrIndex]
                    let arrPrice = singleObj.Price
                    if(Int(arrPrice!)! < less){
                        filteredProduct.append(productDictionary[arrIndex])
                    }
                }
            }
            productDictionary = filteredProduct
        }
        else{
            filteredProduct.removeAll()
        }
        temArrayLoop()
        tblView.reloadData()
    }
    
    func sortedData(_sender : UIButton) {
        if(appDel.productArray.count > 0){
            filteredProduct.removeAll()
            if(sortFlag == 0){
                if(_sender.tag == 0){
                    filteredProduct = productDictionary.sorted(by: {Int($0.Price)! < Int($1.Price)! })
                    productDictionary = filteredProduct
                }
                else{
                    filteredProduct = productDictionary.sorted(by: {Int($0.Price)! > Int($1.Price)! })
                    productDictionary = filteredProduct
                }
            }
            else{
                filteredProduct.removeAll()
            }
        }
        
        temArrayLoop()
        tblView.reloadData()
    }
    
    func temArrayLoop() {
        if(productDictionary.count > 0){
            for _ in 0...(productDictionary.count - 1){
                temArray.append(0)
            }
        }
    }
    
    func cmpArrLoop() {
        if(productDictionary.count > 0){
            for _ in 0...(productDictionary.count - 1){
                cmpArr.append(0)
            }
        }
    }
    
    @objc func cmpProduct(_ sender : UIButton) {
        if(cmpArr[sender.tag] == 0){
            cmpCntr = 0
            for loop in 0...(cmpArr.count - 1){
                if(cmpArr[loop] == 1){
                    cmpCntr += 1
                }
            }
            if(cmpCntr < 2){
                cmpArr[sender.tag] = 1
                let pid = productDictionary[sender.tag].productId
                sendCmpArr.append(pid!)
                btnSubmitCompare.isHidden = false
            }
            else{
                cmpCntr = 0
                alertController(title: "Slected Items", message: "Select only two item", btnName: "Ok")
            }
        }
        else{
            cmpArr[sender.tag] = 0
        }
        tblView.reloadData()
    }
    
    func alertController(title:String, message:String, btnName:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: btnName, style: .default, handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(cmpBtnBool){
            if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
                btnSubmitCompare.isHidden = false
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        btnSubmitCompare.isHidden = true
    }
    
    @objc func testRefresh(_ refreshControl: UIRefreshControl?) {
        if(appDel.productArray.count > 0 ){
            allData = appDel.productArray
            getData()
            stop()
        }
//        if(productDictionary.count == 0){
//            appDel.getProduct()
////             data.getProduct()
//            if(refreshTimer == nil){
//                refreshTimer = Timer.scheduledTimer(timeInterval: 05.0, target: self, selector: #selector(testRefresh(_:)), userInfo: nil, repeats: true)
//            }
//            allData = appDel!.productArray
////            allData = data.productArray
//
//            productDictionary.removeAll()
//            if(allData.count > 0){
//                for loop in 0...(allData.count - 1){
//                    let chkCatId = allData[loop].CatId
//                    if(chkCatId == "1"){
//                        productDictionary.append(allData[loop])
//                    }
//                }
//                stop()
//            }
//            print("timer")
//        }
//        else{
//            alertController(title: "Data", message: "No Update", btnName: "Ok")
//            refreshController.endRefreshing()
//        }
//        tblView.reloadData()
//        if(refreshTimer == nil){
//            refreshTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(stop), userInfo: nil, repeats: true)
//        }
    }
    @objc func stop(){
        refreshController.endRefreshing()
        refreshTimer?.invalidate()
        refreshTimer = nil
        alertController(title: "Data", message: "Updated", btnName: "Ok")
    }
    
    func getData() {
        for loop in 0...(allData.count - 1){
            let chkCatId = allData[loop].CatId
            let chkSubCatId = allData[loop].SubCatId
            if(chkCatId == proCat_Id && chkSubCatId == strSubCatId){
                productDictionary.append(allData[loop])
            }
        }
        if(productDictionary.count > 0){
            temArrayLoop()
            cmpArrLoop()
        }
        tblView.reloadData()
    }
}

