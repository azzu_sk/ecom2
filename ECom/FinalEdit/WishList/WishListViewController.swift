//
//  WishListViewController.swift
//  ECom
//
//  Created by Nan on 08/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import CoreData

class WishListViewController: UIViewController {
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var lblCartBadge: UILabel!
    var appDel:AppDelegate!
    var product = [NSManagedObject]()
    var controllerName : String?
    var imgUrlPath : String!
    struct CartItem {
        var user_id : String!
        var product_id : String!
        var product_name : String!
        var product_b_name : String!
        var product_img : String!
        var product_price : String!
    }
    var itemCartObj : CartItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgUrlPath = urlObj.imgUrlPath
        appDel = UIApplication.shared.delegate as? AppDelegate
        emptyView.isHidden = true
        DispatchQueue.main.async {
            self.activityView.startAnimating()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        product = appDel.likeData
        lblCartBadge.text = String(appDel.cart_Badge)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        check()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        redirectBack(to: backNavigation.last!)
    }
    @IBAction func actionHome(_ sender: Any) {
        redirect(to: "MainScrollViewController")
    }
    @IBAction func actionCart(_ sender: Any) {
        redirect(to: "CartListViewController")
    }
    @IBAction func actionContinueShopping(_ sender: Any) {
        redirect(to: "MainScrollViewController")
    }
    
}

extension WishListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return product.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishListTableViewCell") as! WishListTableViewCell
        let singleObj = product[indexPath.row]
        let img = singleObj.value(forKey: "product_img") as! String
        let imgUrl = URL(string: (imgUrlPath + img))
        let imgData = try? Data(contentsOf: imgUrl!)
        if(imgData == nil){
            cell.productImg.image = UIImage(named: imgUrlPath + img)
        }
        else{
            cell.productImg.image = UIImage(data: imgData!)
        }
        cell.lblName.text = singleObj.value(forKey: "product_name") as? String
        cell.lblBrandName.text = singleObj.value(forKey: "product_b_name") as? String
        cell.lblPrice.text = singleObj.value(forKey: "product_price") as? String
        cell.likeBtn.tag = indexPath.row
        cell.cartBtn.tag = indexPath.row
        cell.likeBtn.addTarget(self, action: #selector(removeWishList(_sender:)), for: .touchUpInside)
        cell.cartBtn.addTarget(self, action: #selector(toCart(_sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let wishId = product[indexPath.row]
        itemControllerProduct_id = wishId.value(forKey: "product_id") as! String
        redirect(to: "ItemDetailViewController")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func redirect(to : String){
        backNavigation.append("WishListViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func redirectBack(to : String){
        backNavigation.removeLast()
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc func removeWishList(_sender : UIButton){
        print(_sender.tag)
        let mngObj = appDel.persistentContainer.viewContext
        mngObj.delete(appDel.likeData[_sender.tag])
        do {
            try mngObj.save()
            appDel.getLike()
            product = appDel.likeData
            check()
        } catch let err as NSError {
            print(err)
        }
    }
    
    @objc func toCart(_sender : UIButton){
         print(_sender.tag)
        let singleObj = appDel.likeData[_sender.tag]
        itemCartObj = CartItem(user_id: singleObj.value(forKey: "user_id") as? String, product_id: singleObj.value(forKey: "product_id") as? String, product_name: singleObj.value(forKey: "product_name") as? String, product_b_name: singleObj.value(forKey: "product_b_name") as? String, product_img: singleObj.value(forKey: "product_img") as? String, product_price: singleObj.value(forKey: "product_price") as? String)
        self.performSegue(withIdentifier: "CartViewController", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "CartViewController"){
            let cartClsObj = segue.destination as! CartViewController
            cartClsObj.product_id = itemCartObj.product_id
            cartClsObj.user_id = itemCartObj.user_id
            cartClsObj.product_name = itemCartObj.product_name
            cartClsObj.product_b_name = itemCartObj.product_b_name
            cartClsObj.product_img = itemCartObj.product_img
            cartClsObj.product_price = itemCartObj.product_price
        }
    }
    
    func check() {
        print(appDel.likeData.count)
        if(appDel.likeData.count <= 0 ){
            tblView.isHidden = true
            emptyView.isHidden = false
        }
        else{
            tblView.isHidden = false
            emptyView.isHidden = true
            product = appDel.likeData
            tblView.reloadData()
        }
        DispatchQueue.main.async {
            self.activityView.stopAnimating()
            self.activityView.isHidden = true
        }
    }
}
