//
//  o1_containerViewController.swift
//  ECom
//
//  Created by Nan on 25/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o1_containerViewController: UIViewController {

    @IBOutlet weak var o1_Collection: UICollectionView!

    var items = ["Mobiles", "Fashion", "Electronics", "Home", "Beauty", "Appliances", "Toys and More", "Books and More"]
    var img = ["mobileToScroll", "fashionTopScroll", "elcetronicsTopScroll", "homeTopScroll", "beautyTopScroll", "appliancesTopScroll", "toysTopScroll", "booksTopScroll"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
}
extension o1_containerViewController:UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "o1_CollectionCell", for: indexPath) as! o1_CollectionCell
        cell.lbl.text = items[indexPath.row]
        cell.img.image = UIImage(named: img[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(items[indexPath.item] == "Mobiles"){
            proCat_Id = "1"
            strSubCatId = "1"
            lblTitleEqualTo = "Mobiles"
            redirect(to: "ElectronicItemViewController")
        }
        else if(items[indexPath.item] == "Fashion"){
            proCat_Id = "3"
            lblTitleEqualTo = "Fashion"
            redirect(to: "FashionViewController")
        }
        else if(items[indexPath.item] == "Electronics"){
            proCat_Id = "1"
            strSubCatId = "2"
            lblTitleEqualTo = "Electronics"
            redirect(to: "ElectronicItemViewController")
        }
        else if(items[indexPath.item] == "Home"){
            proCat_Id = "4"
            strSubCatId = "1"
            lblTitleEqualTo = "Home"
            redirect(to: "ClothingViewController")
        }
        else if(items[indexPath.item] == "Beauty"){
            proCat_Id = "6"
            strSubCatId = "1"
            lblTitleEqualTo = "Beauty"
            redirect(to: "ClothingViewController")
        }
        else if(items[indexPath.item] == "Appliances"){
            proCat_Id = "2"
            strSubCatId = "1"
            lblTitleEqualTo = "Appliances"
            redirect(to: "ElectronicItemViewController")
        }
        else if(items[indexPath.item] == "Toys and More"){
            proCat_Id = "5"
            strSubCatId = "1"
            lblTitleEqualTo = "Toys and More"
            redirect(to: "ClothingViewController")
        }
        else if(items[indexPath.item] == "Books and More"){
            proCat_Id = "7"
            strSubCatId = "1"
            lblTitleEqualTo = "Books and More"
            redirect(to: "ClothingViewController")
        }
    }
    
    func redirect(to : String) {
        backNavigation.append("MainScrollViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
}
