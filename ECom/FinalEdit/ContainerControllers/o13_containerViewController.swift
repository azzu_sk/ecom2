//
//  o13_containerViewController.swift
//  ECom
//
//  Created by Nan on 28/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o13_containerViewController: UIViewController {
    @IBOutlet weak var collection:UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
extension o13_containerViewController:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "o13_CollectionCell", for: indexPath) as! o13_CollectionCell
        return cell
    }
}
