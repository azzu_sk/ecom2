//
//  o4_containerViewController.swift
//  ECom
//
//  Created by Nan on 25/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o4_containerViewController: UIViewController {
    @IBOutlet weak var collection:UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension o4_containerViewController:UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "o4_CollectionCell", for: indexPath) as! o4_CollectionCell
        return cell
    }
}
