//
//  o3_containerViewController.swift
//  ECom
//
//  Created by Nan on 25/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o3_containerViewController: UIViewController {
    

    @IBOutlet weak var collection:UICollectionView!
    var img = ["appliancesTopScroll", "beautyTopScroll", "elcetronicsTopScroll", "beautyTopScroll"]
    var blackLbl = [""]
    var greenlbl = [""]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension o3_containerViewController:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "o3_CollectionCell", for: indexPath) as! o3_CollectionCell
        cell.img.image = UIImage(named: img[indexPath.row])
        return cell
    }
}
