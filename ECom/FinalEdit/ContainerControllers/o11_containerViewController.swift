//
//  o11_containerViewController.swift
//  ECom
//
//  Created by Nan on 28/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o11_containerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension o11_containerViewController:UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "o11_CollectionCell", for: indexPath) as! o11_CollectionCell
        return cell
    }
}
