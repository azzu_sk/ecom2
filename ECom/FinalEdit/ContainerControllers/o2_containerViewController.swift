//
//  o2_containerViewController.swift
//  ECom
//
//  Created by Nan on 25/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o2_containerViewController: UIViewController {

    @IBOutlet weak var o2_Collection: UICollectionView!
    var img = ["appliancesTopScroll", "beautyTopScroll", "elcetronicsTopScroll"]
    var greenlbl = ["Available", "Available", "Available"]
    var blacklbl = ["Appliances", "Beauty", "Electronics"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
extension o2_containerViewController:UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "o2_CollectionCell", for: indexPath) as! o2_CollectionCell
        cell.img.image = UIImage(named: img[indexPath.row])
        cell.lblGreen.text = greenlbl[indexPath.row]
        cell.lblBlack.text = blacklbl[indexPath.row]
        return cell
        
    }
}
