//
//  o15_containerViewController.swift
//  ECom
//
//  Created by Nan on 28/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class o15_containerViewController: UIViewController {

    @IBOutlet weak var collection:UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
extension o15_containerViewController:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "o15_CollectionCell", for: indexPath) as! o15_CollectionCell
        return cell
    }
}
