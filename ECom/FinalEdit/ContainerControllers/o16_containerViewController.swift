//
//  o16_containerViewController.swift
//  ECom
//
//  Created by Nan on 08/04/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

protocol o16_containerProtocol:class {
//    func searchKey(txt:String)
}


class o16_containerViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var appDel:AppDelegate!
    var productDictionary = [Product]()
    weak var delegate: o16_containerProtocol? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appDel = UIApplication.shared.delegate as? AppDelegate
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        productDictionary = appDel.productArray
    }

}

extension o16_containerViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productDictionary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "o16_containerTableViewCell") as! o16_containerTableViewCell
        cell.lblProductName.text = productDictionary[indexPath.row].Name
        return cell
    }
    
    func redirect(to : String){
        backNavigation.append("MainScrollViewController")
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        itemControllerProduct_id = productDictionary[indexPath.row].productId
        strSubCatId = productDictionary[indexPath.row].SubCatId
        proCat_Id = productDictionary[indexPath.row].CatId
        redirect(to: "ItemDetailViewController")
    }
    
    func filterProduct(product:[Product]) {
        print(product)
        if(product.count <= 0){
            productDictionary = appDel.productArray
        }
        else{
            productDictionary = product
        }
        
        tableView.reloadData()
    }
    
}
