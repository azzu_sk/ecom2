//
//  AddProductViewController.swift
//  ECom
//
//  Created by Nan on 01/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import AssetsLibrary
import Alamofire

class AddProductViewController: UIViewController {
    @IBOutlet weak var getItemlbl: UILabel!
    @IBOutlet weak var productName: UITextField!
    @IBOutlet weak var productPrice: UITextField!
    @IBOutlet weak var productDesc: UITextView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgBtn: UIButton!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    var containerCntr = 0
    var containerViewConstraint: NSLayoutConstraint?
    var data1: String?
    var catId = 0
    var items = [Category]()
    var selectItem = ""
    var imgData:Data?
    var imgName = ""
    var addProductUrl : String!
    var appDel : AppDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        addProductUrl = urlObj.upDateData
        productDesc.text = "Enter About Product"
        productDesc.textColor = UIColor.lightGray
        tblView.isHidden = true
        productDesc.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        getItemlbl.isHidden = true
        appDel = UIApplication.shared.delegate as? AppDelegate
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        items = appDel.categoryArray
        getItem(get: data1)
    }
    @IBAction func actionPicImg(_sender: Any){
        let imgPicController = UIImagePickerController()
        imgPicController.delegate = self
        imgPicController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imgPicController, animated: true, completion: nil)
    }
    @IBAction func actionSelectCat(_ sender: Any) {
        tabelHideAndShow()
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        print("actionSubmit")
        if(imgBtn.currentImage == UIImage(named: "default")){
            imgBtn.layer.borderWidth = 2
            imgBtn.layer.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        else{
            imgBtn.layer.borderWidth = 0
        }
        if(productName.text == ""){
            productName.layer.borderWidth = 2
            productName.layer.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        else{
            productName.layer.borderWidth = 0
        }
        if(productPrice.text == ""){
            productPrice.layer.borderWidth = 2
            productPrice.layer.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        else{
            productPrice.layer.borderWidth = 0
        }
        if(productDesc.textColor == UIColor.lightGray){
            productDesc.layer.borderWidth = 2
            productDesc.layer.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        else{
            productDesc.layer.borderWidth = 0
        }
        if(selectBtn.titleLabel?.text == "Select Product Cat"){
            selectBtn.layer.borderWidth = 2
            selectBtn.layer.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        else{
            selectBtn.layer.borderWidth = 0
        }
        if(imgBtn.currentImage == UIImage(named: "default") || productName.text == "" || productPrice.text == "" || productDesc.text == "Enter About Product" || selectBtn.titleLabel?.text == "Select Product Cat"){
            print("nill data")
        }
        else{
            addProduct()
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func getItem(get : String!) {
            if get != nil{
                selectBtn.setTitle(get, for: .normal)
            }
    }
    
    func toBottom() {
        let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
        scrollView.setContentOffset(bottomOffset, animated: true)
    }
    
    func tabelHideAndShow() {
        if(containerCntr == 0){
            tblView.isHidden = false
            containerCntr = 1
            toBottom()
            }else{
            containerCntr = 0
            tblView.isHidden = true
            containerView.frame = CGRect(x: 0, y: 0, width: containerView.frame.width, height: 710)
            scrollView.setContentOffset(.zero, animated: true)
        }
    }
}
extension AddProductViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let imgToData  = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        let imgToSet = imgToData.cgImage
        self.imgData = imgToData.pngData()
        if let imgData = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL{
            ALAssetsLibrary().asset(for: imgData as URL, resultBlock: { (asset) in
                let fileName = asset!.defaultRepresentation()?.filename()
                self.imgName = fileName!
                self.imgBtn.setImage(UIImage(cgImage: imgToSet!), for: .normal)
            }, failureBlock: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if productDesc.textColor == UIColor.lightGray {
            productDesc.text = nil
            productDesc.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if productDesc.text.isEmpty {
            productDesc.text = "Enter About Product"
            productDesc.textColor = UIColor.lightGray
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductCatCell") as! SelectProductCatCell
        cell.lbl.text = items[indexPath.row].cat_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectItem = items[indexPath.row].cat_name
        getItem(get: selectItem)
        catId = Int(items[indexPath.row].cat_id)!
        containerCntr = 1
        tabelHideAndShow()
    }
    
    func addProduct() {
        let details = [
            "product_name": self.productName.text as! String,
            "product_price": self.productPrice.text as! String,
            "product_desc": self.productDesc.text as String,
            "product_category": self.selectBtn.titleLabel?.text as! String,
            "product_cat_id": String(catId)
            ] as [String : String]
        Alamofire.upload(multipartFormData: { (multiPartFormData) in
            multiPartFormData.append(self.imgData!, withName: "fileToUpload", fileName: self.imgName, mimeType: "image/png")
            for (key, value) in details{
                multiPartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: addProductUrl ) { (res) in
            switch res{
            case .success(let upload, _, _):
                upload.uploadProgress(closure:{ (progress) in

                })
                upload.responseString(completionHandler: { (responce) in
                    print(responce)
                    self.appDel.getProduct()
//                    self.data.getProduct()
                    self.alertController(title: "Data", message: "Inserted", actionType: true)
                })
                
            case .failure(_):
                self.alertController(title: "Data", message: "Inserted", actionType: false)
                print("failure")
            }
        }
    }
    
    func redirect(to : String){
//        backNavigation = "AddProductViewController"
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: to)
        self.present(navController, animated: true, completion: nil)
    }
    
    func alertController(title:String, message:String, actionType:Bool) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
            if(actionType){
                self.redirect(to: "MainScrollViewController")
            }
        }
        
        alertController.addAction(alertAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
   
}
