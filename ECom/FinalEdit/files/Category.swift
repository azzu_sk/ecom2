//
//  Category.swift
//  ECom
//
//  Created by Nan on 18/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import Foundation


struct Category {
    var cat_id : String!
    var cat_name : String!
    var cat_icon : String!
}
