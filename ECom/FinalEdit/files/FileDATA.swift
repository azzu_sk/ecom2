//
//  FileDATA.swift
//  ECom
//
//  Created by Nan on 18/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

class FileDATA {
    
//    static let filfe = FileDATA()
  
//  static var categoryArray = [Category]()
//  static var productArray = [Product]()

    var categoryArray = [Category]()
    var productArray = [Product]()
    
     var cartData = [NSManagedObject]()
    var likeData = [NSManagedObject]()
    
    var like_p_id = 0
    var cart_Badge = 0
    var cart_p_id = 0
    var rupee = "\u{20B9}"
    var pricefilter : Float!
    var offline : Bool?
    var refreshBool = false
    var mainObj = MainScrollViewController()
    var urlObj = allUrl()
    var sendImg : Data?
    var window: UIWindow?
    var sendSubImg : Data?
    let app = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    //    var catLink = "http://localhost:8080/ECom/getCategory.php"
    //    var productLink = "http://localhost:8080/ECom/getProduct.php"
    //    var catLink = "https://ecomazzu.000webhostapp.com/ECom/getCategory.php"
    //    var productLink = "https://ecomazzu.000webhostapp.com/ECom/getProduct.php"
    
   public func getCategory() {
        categoryArray.removeAll()
        print("Collecting Category data from server......................................................................")
        Alamofire.request(urlObj.getCatUrl).responseJSON { (responce) in
            print(responce.result.value as Any)
            if let data = responce.result.value as? NSDictionary{
                if let data1 = data["category"] as? NSArray{
                    //                    print(data1)
                    for loop in data1{
                        let dataCategory = loop as? NSDictionary
                        self.categoryArray.append(Category(cat_id: dataCategory?.value(forKey: "category_id") as? String, cat_name: dataCategory?.value(forKey: "category_name") as? String, cat_icon: dataCategory?.value(forKey: "category_icon") as? String))
                    }
                }
                if(self.categoryArray.count > 0 && self.productArray.count > 0){
                    self.changeView(to: "MainScrollViewController")
                }
                else
                {
                    self.getProduct()
                    if(self.categoryArray.count < 0){
                        self.getCategory()
                    }
                    if(self.productArray.count < 0){
                        self.getProduct()
                    }
                }
            }
            else {
                self.offline = true
            }
        }
    }
    
    func getLike(){
        likeData.removeAll()
//        let manObj = persistentContainer.viewContext
        
        let fetchQuery = NSFetchRequest<NSFetchRequestResult>(entityName: "WishList_tb")
        
        do {
            try likeData = app.fetch(fetchQuery) as! [NSManagedObject]
        } catch let err as NSError {
            print(err)
        }
        if(likeData.count > 0){
            like_p_id = (likeData[(likeData.count - 1)].value(forKey: "p_id")) as! Int
            //            print(likeData.count)
        }
    }
    
    func getCart(){
        cartData.removeAll()
//        let manObj = persistentContainer.viewContext
        
        let fetchQuery = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart_tb")
        
        do {
            try cartData = app.fetch(fetchQuery) as! [NSManagedObject]
        } catch let err as NSError {
            print(err)
        }
        if(cartData.count > 0){
            cart_p_id = (cartData[(cartData.count - 1)].value(forKey: "p_id")) as! Int
            cart_Badge = cartData.count
        }
        else{
            cart_Badge = 0
        }
    }
    
    func getProduct()->Void {
        productArray.removeAll()
        print("Collecting Product data from server.......................................................................")
        //        print(urlObj.getProducturl)
        Alamofire.request(urlObj.getProducturl).responseJSON { (responce) in
            print(responce.result.value as Any)
            if let data = responce.result.value as? NSDictionary{
                if let data1 = data["product"] as? NSArray{
                    for loop in data1{
                        let dataProduct = loop as? NSDictionary
                        
                        let imgStr = dataProduct?.value(forKey: "product_img") as? String
                        let imgUrl = URL(string: self.urlObj.imgUrlPath + imgStr!)
                        let imgData = try? Data(contentsOf: imgUrl!)
                        if(imgData == nil){
                            self.sendImg = nil
                        }
                        else{
                            self.sendImg = imgData
                        }
                        
                        self.productArray.append(
                            Product.init(productId: dataProduct?.value(forKey: "p_id") as? String,
                                         image: dataProduct?.value(forKey: "product_img") as? String, BrandName: dataProduct?.value(forKey: "product_brand_name") as? String, Name: dataProduct?.value(forKey: "product_name") as? String, Price: dataProduct?.value(forKey: "product_price") as? String, Desc: dataProduct?.value(forKey: "product_desc") as? String, CatId: dataProduct?.value(forKey: "product_cat_id") as? String, SubCatId: dataProduct?.value(forKey: "product_sub_cat_id") as? String, SubImg : dataProduct?.value(forKey: "product_sub_img") as? String, proImg : self.sendImg)
                        )
                    }
                    //                    print(self.productArray[0])
                    if(self.categoryArray.count > 0 && self.productArray.count > 0){
                        self.changeView(to: "MainScrollViewController")
                    }
                    else{
                        if(self.categoryArray.count < 0){
                            self.getCategory()
                        }
                        if(self.productArray.count < 0){
                            self.getProduct()
                        }
                    }
                }
                self.refreshBool = true
            }
            else {
                //                print(responce.error!)
                
            }
        }
    }
    
    func changeView(to : String){
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: to) as UIViewController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = initialViewControlleripad
        self.window?.makeKeyAndVisible()
    }
    
}
