//
//  Product.swift
//  ECom
//
//  Created by Nan on 18/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import Foundation

struct Product {
    var productId : String!
    var image : String!
    var BrandName : String!
    var Name : String!
    var Price : String!
    var Desc : String!
    var CatId : String!
    var SubCatId : String!
    var SubImg : String!
    var proImg : Data!
}
