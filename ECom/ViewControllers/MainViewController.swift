//
//  MainViewController.swift
//  ECom
//
//  Created by Nan on 24/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var tblView: UITableView!
    var cntr = 0
//    @IBOutlet weak var collectionOne: UICollectionView!
//    @IBOutlet weak var collectionTwo: UICollectionView!
//    @IBOutlet weak var collectionThree: UICollectionView!
    var content = ["title", "content", "title", "content", "title", "content", "title", "content"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblView.delegate = self
        tblView.dataSource = self
        
    }
}
extension MainViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var id = content[indexPath.row]

        print(id)
        
        if(indexPath.row % 2 == 0){
            cntr += 1
        }
        
        if(id == "content"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTwoTblCell") as! ItemTwoTblCell
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemOneTblCell") as! ItemOneTblCell
            cell.txtlabel.text = "item\(cntr)"
//            cntr += 1
            return cell
            
        }
//        if(indexPath.row == 1){
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTwoTblCell") as! ItemTwoTblCell
//            return cell
//        }
//        else if(indexPath.row == 2){
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemThreeTblCell") as! ItemThreeTblCell
//            return cell
//        }
//        return UITableViewCell()
//        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemOneTblCell") as! ItemOneTblCell
//        return cell
    }
    
    
}
