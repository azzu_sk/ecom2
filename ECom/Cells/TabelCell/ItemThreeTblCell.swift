//
//  ItemThreeTblCell.swift
//  ECom
//
//  Created by Nan on 24/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ItemThreeTblCell: UITableViewCell {

    @IBOutlet weak var collectionViewThree: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
extension ItemThreeTblCell:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemThreeCollCell", for: indexPath) as! ItemThreeCollCell
//        cell.imgView.image = #imageLiteral(resourceName: "left")
        return cell
    }
    
    
}

