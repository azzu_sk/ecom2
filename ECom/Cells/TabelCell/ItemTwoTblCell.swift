//
//  ItemTwoTblCell.swift
//  ECom
//
//  Created by Nan on 24/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ItemTwoTblCell: UITableViewCell {

    @IBOutlet weak var collectionViewTwo: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension ItemTwoTblCell:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemTwoCollCell", for: indexPath) as! ItemTwoCollCell
//        cell.imgView.image = UIImage(named: "left")
        return cell
    }
    
    
}
